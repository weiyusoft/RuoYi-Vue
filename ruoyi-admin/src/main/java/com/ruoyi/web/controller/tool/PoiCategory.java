package com.ruoyi.web.controller.tool;

public class PoiCategory
{

    private String name;

    private String child_name;

    private String position;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getChild_name()
    {
        return child_name;
    }

    public void setChild_name(String child_name)
    {
        this.child_name = child_name;
    }

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        this.position = position;
    }
}
