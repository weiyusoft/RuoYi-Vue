package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.framework.web.domain.server.Sys;
import com.ruoyi.system.domain.SysToolsDetail;
import com.ruoyi.system.domain.vo.ProjectStatusVo;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.system.service.ISysToolsDetailService;
import com.ruoyi.system.service.ISysUserService;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysProject;
import com.ruoyi.system.service.ISysProjectService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 项目管理Controller
 *
 * @author zuolei
 * @date 2022-07-30
 */
@RestController
@RequestMapping("/system/project")
public class SysProjectController extends BaseController
{

    @Autowired
    private ISysProjectService sysProjectService;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysDictDataService sysDictDataService;

    @Autowired
    private ISysToolsDetailService sysToolsDetailService;

    /**
     * 查询项目管理列表
     */
//    @PreAuthorize("@ss.hasPermi('system:project:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysProject sysProject)
    {
        //先获取当前用户的id
        Long userId = getUserId();
        //根据当前用户id查询用户
        SysUser user = sysUserService.selectUserById(userId);
        Long deptId = user.getDeptId();
        user = new SysUser();
        user.setUserId(userId);
        user.setDeptId(deptId);
        //根据deptId获取下属
        List<SysUser> managerUsers = sysUserService.selectManageUserList(user);
        managerUsers.add(user);
        Long[] userIds = new Long[managerUsers.size()];
        for (int i = 0; i < managerUsers.size(); i++)
        {
            userIds[i] = managerUsers.get(i).getUserId();
        }
        startPage();
        List<SysProject> list = sysProjectService.selectSysProjectByUserId(userIds);
        return getDataTable(list);
    }

    /**
     * 导出项目管理列表
     */
//    @PreAuthorize("@ss.hasPermi('system:project:export')")
    @Log(title = "项目管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysProject sysProject)
    {
        List<SysProject> list = sysProjectService.selectSysProjectList(sysProject);
        ExcelUtil<SysProject> util = new ExcelUtil<SysProject>(SysProject.class);
        util.exportExcel(response, list, "项目管理数据");
    }

    /**
     * 获取项目管理详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:project:query')")
    @GetMapping(value = "/{projectId}")
    public AjaxResult getInfo(@PathVariable("projectId") Long projectId)
    {
        SysProject sysProject = sysProjectService.selectSysProjectByProjectId(projectId);
        return AjaxResult.success(sysProject);
    }

    /**
     * 新增项目管理
     */
//    @PreAuthorize("@ss.hasPermi('system:project:add')")
    @Log(title = "项目管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysProject sysProject)
    {
        sysProject.setCreateBy(getUsername());
        Long managerId = sysProject.getManager();
        if (managerId != null && managerId != 0)
        {
            SysUser sysUser = sysUserService.selectUserById(managerId);
            sysProject.setManagerName(sysUser.getNickName());
        }
        return toAjax(sysProjectService.insertSysProject(sysProject));
    }

    /**
     * 修改项目管理
     */
//    @PreAuthorize("@ss.hasPermi('system:project:edit')")
    @Log(title = "项目管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysProject sysProject)
    {
        Long managerId = sysProject.getManager();
        if (managerId != null && managerId != 0)
        {
            SysUser sysUser = sysUserService.selectUserById(managerId);
            sysProject.setManagerName(sysUser.getNickName());
        }
        sysProject.setUpdateBy(getUsername());
        return toAjax(sysProjectService.updateSysProject(sysProject));
    }

    /**
     * 修改项目管理
     */
//    @PreAuthorize("@ss.hasPermi('system:project:edit')")
    @Log(title = "项目管理", businessType = BusinessType.UPDATE)
    @PutMapping("/status")
    public AjaxResult editStatus(ProjectStatusVo statusVo)
    {
        return toAjax(sysProjectService.updateProjectStatus(statusVo));
    }

    /**
     * 删除项目管理
     */
//    @PreAuthorize("@ss.hasPermi('system:project:remove')")
    @Log(title = "项目管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{projectIds}")
    public AjaxResult remove(@PathVariable Long[] projectIds)
    {
        return toAjax(sysProjectService.deleteSysProjectByProjectIds(projectIds));
    }
}
