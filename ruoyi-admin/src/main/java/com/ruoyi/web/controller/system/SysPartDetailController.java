package com.ruoyi.web.controller.system;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.system.domain.SysPart;
import com.ruoyi.system.service.ISysPartService;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysPartDetail;
import com.ruoyi.system.service.ISysPartDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 配件详情Controller
 *
 * @author zuolei
 * @date 2022-07-09
 */
@RestController
@RequestMapping("/system/part/detail")
public class SysPartDetailController extends BaseController
{

    @Autowired
    private ISysPartDetailService sysPartDetailService;

    @Autowired
    private ISysPartService sysPartService;

    /**
     * 查询配件详情列表
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysPartDetail sysPartDetail)
    {
        startPage();
        List<SysPartDetail> list = sysPartDetailService.selectSysPartDetailList(sysPartDetail);
        return getDataTable(list);
    }

    /**
     * 导出配件详情列表
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:export')")
    @Log(title = "配件详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysPartDetail sysPartDetail)
    {
        List<SysPartDetail> list = sysPartDetailService.selectSysPartDetailList(sysPartDetail);
        ExcelUtil<SysPartDetail> util = new ExcelUtil<SysPartDetail>(SysPartDetail.class);
        util.exportExcel(response, list, "配件详情数据");
    }

    /**
     * 获取配件详情详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:query')")
    @GetMapping(value = "/{partDetailId}")
    public AjaxResult getInfo(@PathVariable("partDetailId") Long partDetailId)
    {
        return AjaxResult.success(sysPartDetailService.selectSysPartDetailByPartDetailId(partDetailId));
    }


    /**
     * 新增配件详情
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:add')")
    @Log(title = "配件详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysPartDetail sysPartDetail)
    {
        if (null == sysPartDetail.getSubPartId())
        {
            return AjaxResult.error("请设置易损件");
        }

        SysPart sysPart = sysPartService.selectSysPartByPartId(sysPartDetail.getPartId());
        if (null == sysPart)
        {
            return AjaxResult.error("关联配件不存在");
        }

        SysPart childPart = sysPartService.selectSysPartByPartId(sysPartDetail.getSubPartId());
        if (null == childPart)
        {
            return AjaxResult.error("易损件不存在");
        } else if (!childPart.getType().equals(UserConstants.PART_TYPE_2))
        {
            return AjaxResult.error("易损件类型错误");
        }

        SysPartDetail partDetail = sysPartDetailService.selectSysPartDetailBySubPartId(sysPartDetail.getSubPartId());
        if (partDetail != null)
        {
            if (partDetail.getPartId().equals(sysPart.getPartId()))
            {
                return AjaxResult.error("易损件已关联该配件，请勿重复关联");
            }
        }

        sysPartDetail.setCreateBy(getUsername());
        return toAjax(sysPartDetailService.insertSysPartDetail(sysPartDetail));
    }

    /**
     * 修改配件详情
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:edit')")
    @Log(title = "配件详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysPartDetail sysPartDetail)
    {
        return toAjax(sysPartDetailService.updateSysPartDetail(sysPartDetail));
    }

    /**
     * 删除配件详情
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:remove')")
    @Log(title = "配件详情", businessType = BusinessType.DELETE)
    @DeleteMapping("/{partDetailIds}")
    public AjaxResult remove(@PathVariable Long[] partDetailIds)
    {
        sysPartDetailService.deleteSysPartDetailByPartDetailIds(partDetailIds);
        for (long detailId : partDetailIds)
        {
            SysPartDetail sysPartDetail = sysPartDetailService.selectSysPartDetailByPartDetailId(detailId);
            //获取易损件id
            long partId = sysPartDetail.getSubPartId();
            sysPartService.deleteSysPartByPartId(partId);
        }
        return AjaxResult.success();
    }


}
