package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysToolsDetail;
import com.ruoyi.system.service.ISysToolsDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工具管理Controller
 * 
 * @author zuolei
 * @date 2022-08-08
 */
@RestController
@RequestMapping("/system/tools/detail")
public class SysToolsDetailController extends BaseController
{
    @Autowired
    private ISysToolsDetailService sysToolsDetailService;

    /**
     * 查询工具管理列表
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysToolsDetail sysToolsDetail)
    {
        startPage();
        List<SysToolsDetail> list = sysToolsDetailService.selectSysToolsDetailList(sysToolsDetail);
        return getDataTable(list);
    }

    /**
     * 导出工具管理列表
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:export')")
    @Log(title = "工具管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysToolsDetail sysToolsDetail)
    {
        List<SysToolsDetail> list = sysToolsDetailService.selectSysToolsDetailList(sysToolsDetail);
        ExcelUtil<SysToolsDetail> util = new ExcelUtil<SysToolsDetail>(SysToolsDetail.class);
        util.exportExcel(response, list, "工具管理数据");
    }

    /**
     * 获取工具管理详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:query')")
    @GetMapping(value = "/{toolId}")
    public AjaxResult getInfo(@PathVariable("toolId") Long toolId)
    {
        return AjaxResult.success(sysToolsDetailService.selectSysToolsDetailByToolId(toolId));
    }

    /**
     * 新增工具管理
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:add')")
    @Log(title = "工具管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysToolsDetail sysToolsDetail)
    {
        sysToolsDetail.setCreateBy(getUsername());
        sysToolsDetailService.insertSysToolsDetail(sysToolsDetail);
        return AjaxResult.success(sysToolsDetail);
    }

    /**
     * 修改工具管理
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:edit')")
    @Log(title = "工具管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysToolsDetail sysToolsDetail)
    {
        return toAjax(sysToolsDetailService.updateSysToolsDetail(sysToolsDetail));
    }

    /**
     * 删除工具管理
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:remove')")
    @Log(title = "工具管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{toolIds}")
    public AjaxResult remove(@PathVariable Long[] toolIds)
    {
        return toAjax(sysToolsDetailService.deleteSysToolsDetailByToolIds(toolIds));
    }
}
