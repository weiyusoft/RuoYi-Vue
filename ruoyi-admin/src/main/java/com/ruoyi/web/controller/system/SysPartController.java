package com.ruoyi.web.controller.system;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.system.domain.SysPartDetail;
import com.ruoyi.system.service.ISysPartDetailService;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysPart;
import com.ruoyi.system.service.ISysPartService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 配件Controller
 *
 * @author ruoyi
 * @date 2022-07-08
 */
@RestController
@RequestMapping("/system/part")
public class SysPartController extends BaseController
{

    @Autowired
    private ISysPartService sysPartService;

    @Autowired
    private ISysPartDetailService sysPartDetailService;

    /**
     * 查询配件列表
     */
//    @PreAuthorize("@ss.hasPermi('system:part:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysPart sysPart)
    {
        startPage();
        List<SysPart> list = sysPartService.selectSysPartList(sysPart);
        return getDataTable(list);
    }

    /**
     * 导出配件列表
     */
//    @PreAuthorize("@ss.hasPermi('system:part:export')")
    @Log(title = "配件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysPart sysPart)
    {
        List<SysPart> list = sysPartService.selectSysPartList(sysPart);
        ExcelUtil<SysPart> util = new ExcelUtil<SysPart>(SysPart.class);
        util.exportExcel(response, list, "配件数据");
    }

    /**
     * 获取配件详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:part:query')")
    @GetMapping(value = "/{partId}")
    public AjaxResult getInfo(@PathVariable("partId") Long partId)
    {
        return AjaxResult.success(sysPartService.selectSysPartByPartId(partId));
    }

    /**
     * 新增配件
     */
//    @PreAuthorize("@ss.hasPermi('system:part:add')")
    @Log(title = "配件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysPart sysPart)
    {
        String name = sysPart.getName();
        String englishName = sysPart.getEnglishName();
        SysPart zhPart = sysPartService.selectSysPartByName(name);
        SysPart enPart = sysPartService.selectSysPartByName(englishName);
        if (null != zhPart && !UserConstants.DEL_FLAG_DELETE.equals(zhPart.getDelFlag()))
        {
            return AjaxResult.error("新增配件'" + name + "'失败，配件已存在");
        } else if (null != enPart && !UserConstants.DEL_FLAG_DELETE.equals(enPart.getDelFlag()))
        {
            return AjaxResult.error("新增配件'" + englishName + "'失败，配件已存在");
        }

        sysPart.setCreateBy(getUsername());
        Integer type = sysPart.getType();
        Map<String, Object> params = sysPart.getParams();
        int rows = sysPartService.insertSysPart(sysPart);
        if (null == params || params.isEmpty())
        {
            return AjaxResult.success(sysPart);
        } else
        {
            for (Map.Entry<String, Object> entry : params.entrySet())
            {
                Long subPartId = Long.valueOf(entry.getKey());
                SysPart subPart = sysPartService.selectSysPartByPartId(subPartId);
                if (null == subPart)
                {
                    if (type.equals(UserConstants.PART_TYPE_1))
                    {
                        return AjaxResult.error("配件新增成功，但关联易损件失败，易损件不存在");
                    } else if (type.equals(UserConstants.PART_TYPE_2))
                    {
                        return AjaxResult.error("易损件新增成功，但关联配件失败，配件不存在");
                    }
                }

                SysPartDetail partDetail = new SysPartDetail();
                if (type.equals(UserConstants.PART_TYPE_1))
                {
                    partDetail.setPartId(sysPart.getPartId());
                    partDetail.setSubPartId(subPartId);
                } else if (type.equals(UserConstants.PART_TYPE_2))
                {
                    partDetail.setPartId(subPartId);
                    partDetail.setSubPartId(sysPart.getPartId());
                }

                partDetail.setCreateBy(getUsername());
                sysPartDetailService.insertSysPartDetail(partDetail);
            }
            return AjaxResult.success(sysPart);
        }

    }

    /**
     * 修改配件
     */
//    @PreAuthorize("@ss.hasPermi('system:part:edit')")
    @Log(title = "配件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysPart sysPart)
    {
        return toAjax(sysPartService.updateSysPart(sysPart));
    }

    /**
     * 删除配件
     */
//    @PreAuthorize("@ss.hasPermi('system:part:remove')")
    @Log(title = "配件", businessType = BusinessType.DELETE)
    @DeleteMapping("/{partIds}")
    public AjaxResult remove(@PathVariable Long[] partIds)
    {
        sysPartService.deleteSysPartByPartIds(partIds);
        sysPartDetailService.deleteSysPartDetailByPartIds(partIds);
        return AjaxResult.success();
    }
}
