package com.ruoyi.web.controller.system;

import cn.hutool.core.util.HashUtil;
import cn.hutool.core.util.NumberUtil;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.data.MergeCellRule;
import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.data.Pictures;
import com.deepoove.poi.data.RowRenderData;
import com.deepoove.poi.data.Rows;
import com.deepoove.poi.data.TableRenderData;
import com.deepoove.poi.data.Tables;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysPartCategory;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.config.ServerConfig;
import com.ruoyi.system.domain.SysPart;
import com.ruoyi.system.domain.SysPartDetail;
import com.ruoyi.system.domain.SysProduct;
import com.ruoyi.system.domain.SysProductDetail;
import com.ruoyi.system.domain.SysProject;
import com.ruoyi.system.domain.SysToolsDetail;
import com.ruoyi.system.domain.vo.PartVo;
import com.ruoyi.system.domain.vo.ProductPartVo;
import com.ruoyi.system.domain.vo.ProductVo;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.system.service.ISysPartCategoryService;
import com.ruoyi.system.service.ISysPartDetailService;
import com.ruoyi.system.service.ISysPartService;
import com.ruoyi.system.service.ISysProductDetailService;
import com.ruoyi.system.service.ISysProductService;
import com.ruoyi.system.service.ISysProjectService;
import com.ruoyi.system.service.ISysToolsDetailService;
import com.ruoyi.web.controller.tool.PoiCategory;
import com.ruoyi.web.controller.tool.PoiPart;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.ddr.poi.html.HtmlRenderPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 产品/报价单Controller
 *
 * @author zuolei
 * @date 2022-07-25
 */
@RestController
@RequestMapping("/system/product")
public class SysProductController extends BaseController
{

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ISysProductService sysProductService;

    @Autowired
    private ISysPartService sysPartService;

    @Autowired
    private ISysPartCategoryService sysPartCategoryService;

    @Autowired
    private ISysPartDetailService sysPartDetailService;

    @Autowired
    private ISysProductDetailService sysProductDetailService;

    @Autowired
    private ISysProjectService sysProjectService;

    @Autowired
    private ISysDictDataService sysDictDataService;

    @Autowired
    private ISysToolsDetailService sysToolsDetailService;

    /**
     * 查询产品/报价单列表
     */
//    @PreAuthorize("@ss.hasPermi('system:product:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysProduct sysProduct)
    {
        startPage();
        List<SysProduct> list = sysProductService.selectSysProductList(sysProduct);
        return getDataTable(list);
    }

    //    @PreAuthorize("@ss.hasPermi('system:detail:list')")
    @GetMapping("/list/part")
    public AjaxResult partList(SysProduct sysProduct)
    {
        SysProductDetail sysProductDetail = new SysProductDetail();
        sysProductDetail.setProductId(sysProduct.getProductId());
        return AjaxResult.success(getProduct(sysProduct, sysProductDetail));
    }

    private ProductPartVo getProduct(SysProduct sysProduct, SysProductDetail sysProductDetail)
    {
        List<SysProductDetail> list = sysProductDetailService.selectSysProductDetailList(sysProductDetail);
        SysProduct product = sysProductService.selectSysProductByProductId(sysProductDetail.getProductId());
        List<PartVo> partList = new ArrayList<>();
        long partCount = 0;
        long consumablePartCount = 0;
        long customPart = 0;
        for (SysProductDetail detail : list)
        {
            PartVo partVo = new PartVo();
            //获取每个partId
            Long partId = detail.getPartId();
            //查询配件信息
            SysPart sysPart = sysPartService.selectSysPartByPartId(partId);

            if (detail.getPartType() == UserConstants.PART_TYPE_1)
            {
                partCount = (long) NumberUtil.add(partCount, detail.getPartCount().longValue());
            } else if (detail.getPartType() == UserConstants.PART_TYPE_2)
            {
                consumablePartCount = (long) NumberUtil.add(consumablePartCount, detail.getPartCount().longValue());
                SysPartDetail sysPartDetail = sysPartDetailService.selectSysPartDetailBySubPartId(partId);
                if (sysPartDetail == null)
                {
                    continue;
                }
                partVo.setParentPartId(sysPartDetail.getPartId());
            } else if (detail.getPartType() == UserConstants.PART_TYPE_3)
            {
                customPart = (long) NumberUtil.add(customPart, detail.getPartCount().longValue());
            }

            if (sysProduct.getExport() != null)
            {
                if (!sysPart.getExport().equals(sysProduct.getExport()))
                {
                    continue;
                }
            }

            partVo.setPartId(sysPart.getPartId());
            partVo.setCount(detail.getPartCount());
            partVo.setName(sysPart.getName());
            partVo.setType(sysPart.getType());
            partVo.setEnglishName(sysPart.getEnglishName());
            partVo.setSubPartEnable(sysPart.getSubPartEnable());
            partVo.setManufacturer(sysPart.getManufacturer());
            partList.add(partVo);
        }
        ProductVo productVo = new ProductVo();
        productVo.setProductId(product.getProductId());
        productVo.setPrice(product.getPrice());
        productVo.setPartCount(partCount);
        productVo.setConsumablePartCount(consumablePartCount);
        productVo.setCustomPartCount(customPart);
        ProductPartVo productPartVo = new ProductPartVo();
        productPartVo.setProduct(productVo);
        productPartVo.setPartList(partList);
        return productPartVo;
    }

    /**
     * 导出产品/报价单列表
     */
//    @PreAuthorize("@ss.hasPermi('system:product:export')")
    @Log(title = "产品/报价单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HttpServletResponse response, SysProduct sysProduct)
    {
        SysProduct product = sysProductService.selectSysProductByProductId(sysProduct.getProductId());
        SysProject project = sysProjectService.selectSysProjectByProjectId(product.getProjectId());
        if (null == project)
        {
            return AjaxResult.error("报价单无关联项目");
        }
        SysProductDetail sysProductDetail = new SysProductDetail();
        sysProductDetail.setProductId(sysProduct.getProductId());
        List<SysProductDetail> list = sysProductDetailService.selectSysProductDetailList(sysProductDetail);
        List<SysPartCategory> categories = new ArrayList<>();
        List<SysPartCategory> childCategories = new ArrayList<>();
        List<String> categoryNames = new ArrayList<>();
        List<SysPart> partList = new ArrayList<>();
        List<PoiPart> mainPartList = new ArrayList<>();
        List<PoiPart> consumablePartList = new ArrayList<>();
        List<PoiPart> customPartList = new ArrayList<>();
        for (SysProductDetail productDetail : list)
        {
            //查询配件信息
            Long partId = productDetail.getPartId();
            SysPart sysPart = sysPartService.selectSysPartByPartId(partId);
            if (sysPart == null)
            {
                continue;
            }

            PoiPart tablePart = new PoiPart();
            tablePart.setPart_name(sysPart.getName());
            tablePart.setPart_no(sysPart.getPartNo());
            tablePart.setCount(productDetail.getPartCount() + "");
            tablePart.setManufacturer(sysPart.getManufacturer());
            tablePart.setCategoryId(sysPart.getCategoryId());
            if ("0".equals(sysPart.getExport()))
            {
                if (productDetail.getPartType() == UserConstants.PART_TYPE_1)
                {
                    mainPartList.add(tablePart);
                } else if (productDetail.getPartType() == UserConstants.PART_TYPE_2)
                {
                    consumablePartList.add(tablePart);
                } else if (productDetail.getPartType() == UserConstants.PART_TYPE_3)
                {
                    customPartList.add(tablePart);
                }
            }

            SysPartCategory category = sysPartCategoryService.selectSysPartCategoryByCategoryId(
                sysPart.getCategoryId());
            if (category == null)
            {
                continue;
            }
            if (category.getParentId() == 0)
            {
                continue;
            }

            partList.add(sysPart);
            String[] splitIds = category.getAncestors().split(",");
            List<String> categoryParentIds = new ArrayList<>();
            for (String id : splitIds)
            {
                categoryParentIds.add(id);
            }
            categoryParentIds.add(category.getCategoryId().toString());
            for (String categoryId : categoryParentIds)
            {
                if (categoryId.equals("0") || categoryId.equals("1"))
                {
                    continue;
                }

                category = sysPartCategoryService.selectSysPartCategoryByCategoryId(Long.parseLong(categoryId));
                //计算分类达到三级，不导出
                String ancestors = category.getAncestors();
                String[] ids = ancestors.split(",");
                if (ids.length > 3)
                {
                    continue;
                }

                if (category.getParentId() == 1)
                {
                    if (!categoryNames.contains(category.getName()))
                    {
                        categories.add(category);
                        categoryNames.add(category.getName());
                    }
                } else
                {
                    if (!categoryNames.contains(category.getName()))
                    {
                        categoryNames.add(category.getName());
                        childCategories.add(category);
                    }
                }
            }
            Collections.sort(mainPartList);
        }

        List<String> firstLevelNames = new ArrayList<>();
        List<PoiCategory> poiCategoryList = new ArrayList<>();
        Map<String, List<SysPart>> resultMap = new LinkedHashMap<>();
        for (int i = 0; i < categories.size(); i++)
        {
            SysPartCategory category = categories.get(i);
            //构建POI分类数据：包含子分类
            PoiCategory poiCategory = new PoiCategory();
            poiCategory.setPosition((i + 1) + "");
            poiCategory.setName(category.getName());
            StringBuilder childName = new StringBuilder();
            for (SysPartCategory childCategory : childCategories)
            {
                String[] ancestors = childCategory.getAncestors().split(",");
                ArrayList<String> ids = new ArrayList<>();
                for (String id : ancestors)
                {
                    if (!"0".equals(id) && !"1".equals(id))
                    {
                        ids.add(id);
                    }
                }
                //找到子分类
                if (ids.contains(category.getCategoryId().toString()))
                {
                    if (!childName.toString().isEmpty())
                    {
                        childName.append("、" + childCategory.getName());
                    } else
                    {
                        childName.append(childCategory.getName());
                    }
                }
            }
            poiCategory.setChild_name(childName.toString());
            poiCategoryList.add(poiCategory);

            //构建分类下配件组合
            List<SysPart> resultParts = new ArrayList<>();
            for (SysPart part : partList)
            {
                if ("2".equals(part.getExport()))
                {
                    System.out.println("配件：" + part.getName() + "不可导出");
                    continue;
                }
                Long childCategoryId = part.getCategoryId();
                SysPartCategory childCategory = sysPartCategoryService.selectSysPartCategoryByCategoryId(
                    childCategoryId);
                String[] splitIds = childCategory.getAncestors().split(",");
                String categoryId = String.valueOf(category.getCategoryId());
                for (String id : splitIds)
                {
                    if (id.equals(categoryId))
                    {
                        resultParts.add(part);
                    }
                }
            }
            firstLevelNames.add(category.getName());
            resultMap.put(category.getName(), resultParts);
        }

        //-------------------------------------------------分·割·线------------------------------------------------------
        //构造配件数据
        List<PoiPart> poiPartList = new ArrayList<>();

        for (Map.Entry<String, List<SysPart>> entry : resultMap.entrySet())
        {
            String categoryName = entry.getKey();
            List<SysPart> mapParts = entry.getValue();
            Collections.sort(mapParts);
            int index = firstLevelNames.indexOf(categoryName);
            for (int i = 0; i < mapParts.size(); i++)
            {
                PoiPart poiCategory = new PoiPart();
                if (i == 0)
                {
                    poiCategory.setPosition("4." + (index + 1) + " ");
                    poiCategory.setCategory_name(categoryName);
                }
                poiCategory.setPart_name("4." + (index + 1) + "." + (i + 1) + " " + mapParts.get(i).getName());
                if (StringUtils.isNotEmpty(mapParts.get(i).getPicture()))
                {
                    poiCategory.setPicture(Pictures.ofUrl(mapParts.get(i).getPicture()).size(200, 200).create());
                }
                poiCategory.setDescription(mapParts.get(i).getDescription());
                poiPartList.add(poiCategory);
            }
        }

        //-------------------------------------------------分·割·线------------------------------------------------------
        //构造主要配件表格数据
        RowRenderData[] mainRows = new RowRenderData[mainPartList.size() + 1];
        RowRenderData row0 = Rows.of("序号", "型号", "主要配置名称", "数量", "制造商").textColor("000000").bgColor("ffffff").center()
            .create();
        mainRows[0] = row0;
        for (int i = 0; i < mainPartList.size(); i++)
        {
            PoiPart part = mainPartList.get(i);
            String oldName = part.getPart_name();
            String newName = oldName;
            String modelNo = "";
            if (oldName.contains("/"))
            {
                String[] names = oldName.split("/");
                newName = names[0];
                modelNo = names[1];
            }
            RowRenderData row = Rows.of(String.valueOf(i + 1), modelNo, newName, part.getCount(),
                    part.getManufacturer())
                .center()
                .textColor("000000")
                .bgColor("ffffff")
                .create();
            mainRows[i + 1] = row;
        }
        TableRenderData mainPartData = Tables.create(mainRows);

        //-------------------------------------------------分·割·线------------------------------------------------------
        //构造易损件表格数据
        RowRenderData[] consumablePartRows = new RowRenderData[consumablePartList.size() + 1];
        RowRenderData consumablePartRow0 = Rows.of("序号", "规格或型号", "名称与描述", "数量", "制造商").textColor("000000")
            .bgColor("ffffff")
            .center()
            .create();
        consumablePartRows[0] = consumablePartRow0;
        for (int i = 0; i < consumablePartList.size(); i++)
        {
            PoiPart part = consumablePartList.get(i);
            String oldName = part.getPart_name();
            String newName = oldName;
            String modelNo = "";
            if (oldName.contains("/"))
            {
                String[] names = oldName.split("/");
                newName = names[0];
                modelNo = names[1];
            }
            RowRenderData row = Rows.of(String.valueOf(i + 1), modelNo, newName, part.getCount(),
                    part.getManufacturer())
                .center()
                .textColor("000000")
                .bgColor("ffffff")
                .create();
            consumablePartRows[i + 1] = row;
        }
        TableRenderData consumablePartData = Tables.create(consumablePartRows);

        //-------------------------------------------------分·割·线------------------------------------------------------
        //构造工具清单表格数据
        SysToolsDetail sysToolsDetail = new SysToolsDetail();
        Long toolsId = project.getTools();
        sysToolsDetail.setParentId(toolsId);
        List<SysToolsDetail> sysToolsDetails = sysToolsDetailService.selectSysToolsDetailList(sysToolsDetail);
        RowRenderData[] toolsRows = new RowRenderData[sysToolsDetails.size() + 1];
        RowRenderData toolsRow0 = Rows.of("序号", "工具名称", "规格或型号", "数量").textColor("000000").bgColor("ffffff")
            .center()
            .create();
        toolsRows[0] = toolsRow0;
        for (int i = 0; i < sysToolsDetails.size(); i++)
        {
            SysToolsDetail detail = sysToolsDetails.get(i);
            RowRenderData row = Rows.of(String.valueOf(i + 1), detail.getToolName(), detail.getToolNo(),
                    detail.getToolCount() + "")
                .center()
                .textColor("000000")
                .bgColor("ffffff")
                .create();
            toolsRows[i + 1] = row;
        }
        TableRenderData toolsData = Tables.create(toolsRows);

        //-------------------------------------------------分·割·线------------------------------------------------------
        //构造自定义物料
        RowRenderData[] customPartRows = new RowRenderData[customPartList.size() + 1];
        RowRenderData customPartRow0 = Rows.of("序号", "规格或型号", "名称与描述", "数量", "制造商").textColor("000000")
            .bgColor("ffffff")
            .center()
            .create();
        customPartRows[0] = customPartRow0;
        for (int i = 0; i < customPartList.size(); i++)
        {
            PoiPart part = customPartList.get(i);
            String oldName = part.getPart_name();
            String newName = oldName;
            String modelNo = "";
            if (oldName.contains("/"))
            {
                String[] names = oldName.split("/");
                newName = names[0];
                modelNo = names[1];
            }
            RowRenderData row = Rows.of(String.valueOf(i + 1), modelNo, newName, part.getCount(),
                    part.getManufacturer())
                .center()
                .textColor("000000")
                .bgColor("ffffff")
                .create();
            customPartRows[i + 1] = row;
        }
        TableRenderData customPartData = Tables.create(customPartRows);
        //构造富文本
        HtmlRenderPolicy htmlRenderPolicy = new HtmlRenderPolicy();
        Configure configure = Configure.builder()
            .bind("description", htmlRenderPolicy)
            .bind("project_description", htmlRenderPolicy)
            .bind("conditions", htmlRenderPolicy)
            .bind("machine_standard", htmlRenderPolicy)
            .bind("application_scope", htmlRenderPolicy)
            .bind("ability", htmlRenderPolicy)
            .build();

        Map<String, Object> data = new HashMap<>();
        data.put("title", project.getName());
        data.put("project_no", project.getProjectNo());
        data.put("product_no", product.getProductNo());
        data.put("create_time", product.getCreateTime());
        data.put("project_description", project.getDescription());
        data.put("machine_standard", project.getStandardValue());
        data.put("application_scope", project.getApplicationscope());
        data.put("conditions", project.getConditionsValue());
        data.put("ability", project.getAbility());
        data.put("company_name", "艾美特焊接自动化");

        PictureRenderData logo = Pictures.ofUrl("http://221.226.33.138:81/profile/upload/logo.jpg").size(60, 30)
            .create();
        data.put("logo", logo);
        //构造系统布局
        SysDictData layoutData = sysDictDataService.selectDictDataById(project.getLayout());
        if (StringUtils.isNotEmpty(layoutData.getDictValue()))
        {
            PictureRenderData layout = Pictures.ofUrl(layoutData.getDictValue()).size(200, 200).create();
            data.put("layout", layout);
        }
        data.put("category", poiPartList);
        data.put("categories", poiCategoryList);
        if (!mainPartList.isEmpty())
        {

            data.put("main_part", mainPartData);
        }
        if (!consumablePartList.isEmpty())
        {
            data.put("consumable_part_title", "附件、备品备件清单");
            data.put("consumable_part", consumablePartData);
        }

        if (!sysToolsDetails.isEmpty())
        {
            data.put("tools_title", "附件、专用工具清单");
            data.put("tools", toolsData);
        }

        if (!customPartList.isEmpty())
        {
            data.put("custom_part_title", "附件、自定义物料");
            data.put("custom_part", customPartData);
        }

        XWPFTemplate template = XWPFTemplate.compile("F:/amet/export/template.docx", configure).render(data);
        FileOutputStream out2 = null;
        String fileName = "F:/amet/export/" + project.getName() + "-" + project.getProjectNo() + ".docx";
        try
        {
            out2 = new FileOutputStream(fileName);
            template.write(out2);
            out2.flush();
            out2.close();
            template.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        File file = new File(fileName);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("url", serverConfig.getUrl() + "-export-" + file.getAbsolutePath());
        return ajax;
    }

    /**
     * 获取产品/报价单详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:product:query')")
    @GetMapping(value = "/{productId}")
    public AjaxResult getInfo(@PathVariable("productId") Long productId)
    {
        return AjaxResult.success(sysProductService.selectSysProductByProductId(productId));
    }

    /**
     * 新增产品/报价单
     */
//    @PreAuthorize("@ss.hasPermi('system:product:add')")
    @Log(title = "产品/报价单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysProduct sysProduct)
    {
        String name = sysProduct.getName();
        String englishName = sysProduct.getEnglishName();
        SysProduct zhProduct = sysProductService.selectSysProductByName(name);
        SysProduct enProduct = sysProductService.selectSysProductByName(englishName);
        if (zhProduct != null)
        {
            return AjaxResult.error("新增产品/报检单'" + name + "'失败，产品/报价单名称已存在");
        } else if (enProduct != null)
        {
            return AjaxResult.error("新增产品/报检单'" + englishName + "'失败，产品/报价单名称已存在");
        }
        if (UserConstants.PRODUCT_TYPE_2.equals(sysProduct.getType()))
        {
            Long projectId = sysProduct.getProjectId();
            SysProduct queryProduct = new SysProduct();
            queryProduct.setProjectId(projectId);
            List<SysProduct> projects = sysProductService.selectSysProductList(queryProduct);
            if (null == projects || projects.isEmpty())
            {
                sysProduct.setProductNo("1");
            } else
            {
                int productNo = projects.size() + 1;
                sysProduct.setProductNo(String.valueOf(productNo));
            }
        }
        sysProduct.setCreateBy(getUsername());
        sysProductService.insertSysProduct(sysProduct);
        Map<String, Object> params = sysProduct.getParams();
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Map.Entry<String, Object> entry : params.entrySet())
        {
            //配件id
            Long partId = Long.valueOf(entry.getKey());
            //配件数量
            Integer count = (Integer) entry.getValue();

            SysPart sysPart = sysPartService.selectSysPartByPartId(partId);
            int type = sysPart.getType();
            SysProductDetail productDetail = new SysProductDetail();
            productDetail.setProductId(sysProduct.getProductId());
            productDetail.setProductNo(sysProduct.getProductNo());
            productDetail.setCreateBy(sysProduct.getCreateBy());
            productDetail.setPartType(type);
            productDetail.setPartId(partId);
            productDetail.setPartCount(count.longValue());
            BigDecimal mul = NumberUtil.mul(sysPart.getPrice(), count);
            productDetail.setPrice(mul);
            totalPrice = NumberUtil.add(totalPrice, mul);
            sysProductDetailService.insertSysProductDetail(productDetail);
        }
        sysProduct.setPrice(totalPrice);
        sysProductService.updateSysProduct(sysProduct);
        return AjaxResult.success(sysProduct);
    }

    /**
     * 修改产品/报价单
     */
//    @PreAuthorize("@ss.hasPermi('system:product:edit')")
    @Log(title = "产品/报价单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysProduct sysProduct)
    {
        sysProductDetailService.deleteSysProductDetailByProductId(sysProduct.getProductId());
        sysProduct.setUpdateBy(getUsername());
        Map<String, Object> params = sysProduct.getParams();
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Map.Entry<String, Object> entry : params.entrySet())
        {
            //配件id
            Long partId = Long.valueOf(entry.getKey());
            //配件数量
            Integer count = (Integer) entry.getValue();

            SysPart sysPart = sysPartService.selectSysPartByPartId(partId);
            int type = sysPart.getType();
            SysProductDetail productDetail = new SysProductDetail();
            productDetail.setProductId(sysProduct.getProductId());
            productDetail.setProductNo(sysProduct.getProductNo());
            productDetail.setCreateBy(sysProduct.getCreateBy());
            productDetail.setPartType(type);
            productDetail.setPartId(partId);
            productDetail.setPartCount(count.longValue());
            BigDecimal mul = NumberUtil.mul(sysPart.getPrice(), count);
            productDetail.setPrice(mul);
            totalPrice = NumberUtil.add(totalPrice, mul);
            sysProductDetailService.insertSysProductDetail(productDetail);
        }
        sysProduct.setPrice(totalPrice);
        sysProductService.updateSysProduct(sysProduct);
        return AjaxResult.success();
    }

    /**
     * 删除产品/报价单
     */
//    @PreAuthorize("@ss.hasPermi('system:product:remove')")
    @Log(title = "产品/报价单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{productIds}")
    public AjaxResult remove(@PathVariable Long[] productIds)
    {
        return toAjax(sysProductService.deleteSysProductByProductIds(productIds));
    }
}
