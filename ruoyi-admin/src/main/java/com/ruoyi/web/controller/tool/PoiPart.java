package com.ruoyi.web.controller.tool;


import com.deepoove.poi.data.PictureRenderData;
import org.jetbrains.annotations.NotNull;

public class PoiPart implements Comparable<PoiPart>
{

    private String position;
    private String category_name;
    private String part_name;
    private PictureRenderData picture;
    private String description;
    private String count;
    private String manufacturer;
    private String part_no;
    private Long categoryId;

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getCategory_name()
    {
        return category_name;
    }

    public void setCategory_name(String category_name)
    {
        this.category_name = category_name;
    }

    public String getPart_name()
    {
        return part_name;
    }

    public void setPart_name(String part_name)
    {
        this.part_name = part_name;
    }

    public PictureRenderData getPicture()
    {
        return picture;
    }

    public void setPicture(PictureRenderData picture)
    {
        this.picture = picture;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getCount()
    {
        return count;
    }

    public void setCount(String count)
    {
        this.count = count;
    }

    public String getPart_no()
    {
        return part_no;
    }

    public void setPart_no(String part_no)
    {
        this.part_no = part_no;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    @Override
    public int compareTo(@NotNull PoiPart o)
    {
        return (int) (this.categoryId-o.categoryId);
    }
}
