package com.ruoyi.web.controller.system;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.system.domain.SysPart;
import com.ruoyi.system.service.ISysPartService;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.domain.entity.SysPartCategory;
import com.ruoyi.system.service.ISysPartCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 配件分类Controller
 *
 * @author zuolei
 * @date 2022-07-08
 */
@RestController
@RequestMapping("/system/category")
public class SysPartCategoryController extends BaseController
{

    @Autowired
    private ISysPartCategoryService sysPartCategoryService;

    @Autowired
    private ISysPartService sysPartService;

    /**
     * 查询配件分类列表
     */
//    @PreAuthorize("@ss.hasPermi('system:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysPartCategory sysPartCategory)
    {
        List<SysPartCategory> list = sysPartCategoryService.selectSysPartCategoryList(sysPartCategory);
        return getDataTable(list);
    }

    /**
     * 获取配件下拉树列表
     */
    @PostMapping("/treeselect")
    public AjaxResult treeselect(SysPartCategory sysPartCategory)
    {
        List<SysPartCategory> categories = sysPartCategoryService.selectSysPartCategoryList(sysPartCategory);
        return AjaxResult.success(sysPartCategoryService.buildCategoryTreeSelect(categories));
    }

    /**
     * 导出配件分类列表
     */
//    @PreAuthorize("@ss.hasPermi('system:category:export')")
    @Log(title = "配件分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysPartCategory sysPartCategory)
    {
        List<SysPartCategory> list = sysPartCategoryService.selectSysPartCategoryList(sysPartCategory);
        ExcelUtil<SysPartCategory> util = new ExcelUtil<SysPartCategory>(SysPartCategory.class);
        util.exportExcel(response, list, "配件分类数据");
    }

    /**
     * 获取配件分类详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:category:query')")
    @GetMapping(value = "/{categoryId}")
    public AjaxResult getInfo(@PathVariable("categoryId") Long categoryId)
    {
        return AjaxResult.success(sysPartCategoryService.selectSysPartCategoryByCategoryId(categoryId));
    }

    /**
     * 新增配件分类
     */
//    @PreAuthorize("@ss.hasPermi('system:category:add')")
    @Log(title = "配件分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysPartCategory sysPartCategory)
    {
        String name = sysPartCategory.getName();
        String englishName = sysPartCategory.getEnglishName();
        SysPartCategory zhCategory = sysPartCategoryService.selectSysPartCategoryByName(name);
        SysPartCategory enCategory = sysPartCategoryService.selectSysPartCategoryByName(
            String.valueOf(englishName).toLowerCase(Locale.ROOT));
        if (null != zhCategory && !UserConstants.DEL_FLAG_DELETE.equals(zhCategory.getDelFlag()))
        {
            return AjaxResult.error("新增配件分类'" + name + "'失败，分类已存在");
        } else if (null != enCategory && !UserConstants.DEL_FLAG_DELETE.equals(enCategory.getDelFlag()))
        {
            return AjaxResult.error("新增配件分类'" + englishName + "'失败，分类已存在");
        }
        sysPartCategory.setCreateBy(getUsername());
        return toAjax(sysPartCategoryService.insertSysPartCategory(sysPartCategory));
    }

    /**
     * 修改配件分类
     */
//    @PreAuthorize("@ss.hasPermi('system:category:edit')")
    @Log(title = "配件分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysPartCategory sysPartCategory)
    {
        return toAjax(sysPartCategoryService.updateSysPartCategory(sysPartCategory));
    }

    /**
     * 删除配件分类
     */
//    @PreAuthorize("@ss.hasPermi('system:category:remove')")
    @Log(title = "配件分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{categoryIds}")
    public AjaxResult remove(@PathVariable Long[] categoryIds)
    {
        SysPart sysPart = new SysPart();
        for (Long categoryId : categoryIds)
        {
            sysPart.setCategoryId(categoryId);
            List<SysPart> list = sysPartService.selectSysPartList(sysPart);
            if (null != list && !list.isEmpty())
            {
                for (SysPart part : list)
                {
                    if (!UserConstants.DEL_FLAG_DELETE.equals(part.getDelFlag()))
                    {
                        return AjaxResult.error("分类下有配件，无法删除");
                    }
                }
            }
        }
        return toAjax(sysPartCategoryService.deleteSysPartCategoryByCategoryIds(categoryIds));
    }
}
