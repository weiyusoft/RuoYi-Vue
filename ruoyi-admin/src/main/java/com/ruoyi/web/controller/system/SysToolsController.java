package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysTools;
import com.ruoyi.system.service.ISysToolsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工具清单管理Controller
 *
 * @author zuolei
 * @date 2022-08-08
 */
@RestController
@RequestMapping("/system/tools")
public class SysToolsController extends BaseController
{

    @Autowired
    private ISysToolsService sysToolsService;

    /**
     * 查询工具清单管理列表
     */
//    @PreAuthorize("@ss.hasPermi('system:tools:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysTools sysTools)
    {
        startPage();
        List<SysTools> list = sysToolsService.selectSysToolsList(sysTools);
        return getDataTable(list);
    }

    /**
     * 导出工具清单管理列表
     */
//    @PreAuthorize("@ss.hasPermi('system:tools:export')")
    @Log(title = "工具清单管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysTools sysTools)
    {
        List<SysTools> list = sysToolsService.selectSysToolsList(sysTools);
        ExcelUtil<SysTools> util = new ExcelUtil<SysTools>(SysTools.class);
        util.exportExcel(response, list, "工具清单管理数据");
    }

    /**
     * 获取工具清单管理详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:tools:query')")
    @GetMapping(value = "/{toolsId}")
    public AjaxResult getInfo(@PathVariable("toolsId") Long toolsId)
    {
        return AjaxResult.success(sysToolsService.selectSysToolsByToolsId(toolsId));
    }

    /**
     * 新增工具清单管理
     */
//    @PreAuthorize("@ss.hasPermi('system:tools:add')")
    @Log(title = "工具清单管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTools sysTools)
    {
        sysTools.setCreateBy(getUsername());
        sysToolsService.insertSysTools(sysTools);
        return AjaxResult.success(sysTools);
    }

    /**
     * 修改工具清单管理
     */
//    @PreAuthorize("@ss.hasPermi('system:tools:edit')")
    @Log(title = "工具清单管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTools sysTools)
    {
        return toAjax(sysToolsService.updateSysTools(sysTools));
    }

    /**
     * 删除工具清单管理
     */
//    @PreAuthorize("@ss.hasPermi('system:tools:remove')")
    @Log(title = "工具清单管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{toolsIds}")
    public AjaxResult remove(@PathVariable Long[] toolsIds)
    {
        return toAjax(sysToolsService.deleteSysToolsByToolsIds(toolsIds));
    }
}
