package com.ruoyi.web.controller.system;

import com.ruoyi.system.domain.SysPart;
import com.ruoyi.system.domain.SysProduct;
import com.ruoyi.system.domain.vo.ProductPartVo;
import com.ruoyi.system.service.ISysPartService;
import com.ruoyi.system.service.ISysProductService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysProductDetail;
import com.ruoyi.system.service.ISysProductDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品/报价单关联详情Controller
 *
 * @author zuolei
 * @date 2022-07-25
 */
@RestController
@RequestMapping("/system/product/detail")
public class SysProductDetailController extends BaseController
{

    @Autowired
    private ISysProductDetailService sysProductDetailService;

    @Autowired
    private ISysProductService sysProductService;

    @Autowired
    private ISysPartService sysPartService;

    /**
     * 查询产品/报价单关联详情列表
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysProductDetail sysProductDetail)
    {
        startPage();
        List<SysProductDetail> list = sysProductDetailService.selectSysProductDetailList(sysProductDetail);
        return getDataTable(list);
    }

    /**
     * 导出产品/报价单关联详情列表
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:export')")
    @Log(title = "产品/报价单关联详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysProductDetail sysProductDetail)
    {
        List<SysProductDetail> list = sysProductDetailService.selectSysProductDetailList(sysProductDetail);
        ExcelUtil<SysProductDetail> util = new ExcelUtil<SysProductDetail>(SysProductDetail.class);
        util.exportExcel(response, list, "产品/报价单关联详情数据");
    }

    /**
     * 获取产品/报价单关联详情详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:query')")
    @GetMapping(value = "/{productDetailId}")
    public AjaxResult getInfo(@PathVariable("productDetailId") Long productDetailId)
    {
        return AjaxResult.success(sysProductDetailService.selectSysProductDetailByProductDetailId(productDetailId));
    }

    /**
     * 新增产品/报价单关联详情
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:add')")
    @Log(title = "产品/报价单关联详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysProductDetail sysProductDetail)
    {
        return toAjax(sysProductDetailService.insertSysProductDetail(sysProductDetail));
    }

    /**
     * 修改产品/报价单关联详情
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:edit')")
    @Log(title = "产品/报价单关联详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysProductDetail sysProductDetail)
    {
        return toAjax(sysProductDetailService.updateSysProductDetail(sysProductDetail));
    }

    /**
     * 删除产品/报价单关联详情
     */
//    @PreAuthorize("@ss.hasPermi('system:detail:remove')")
    @Log(title = "产品/报价单关联详情", businessType = BusinessType.DELETE)
    @DeleteMapping("/{productDetailIds}")
    public AjaxResult remove(@PathVariable Long[] productDetailIds)
    {
        return toAjax(sysProductDetailService.deleteSysProductDetailByProductDetailIds(productDetailIds));
    }
}
