package com.ruoyi.common.utils;

import cn.hutool.core.codec.Base64;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.utils.sign.Md5Utils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestTemplateUtil
{

    public static String SMS_URL = "http://www.js139.com.cn:8022/sms/smssend";

    private static HttpHeaders buildHeader(String sign)
    {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
        if (StringUtils.isNotEmpty(sign))
        {
            httpHeaders.add("Authorization", sign);
        }
        return httpHeaders;
    }


    /**
     * @param jsonString 请求参数
     * @return
     */
    public static String doPost(String apiKey, String jsonString)
    {
        String time = String.valueOf(System.currentTimeMillis());
        String secretKey = "a28e32dd8d97458b88580683b21b908d";
        String signStr = Md5Utils.hash(apiKey + secretKey + time);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("apiKey", apiKey);
        jsonObject.put("time", time);
        jsonObject.put("sign", signStr);
        String realSign = Base64.encode(jsonObject.toJSONString());

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Object> requestEntity = new HttpEntity<>(jsonString, buildHeader(realSign));
        ResponseEntity<String> resp = restTemplate.postForEntity(SMS_URL, requestEntity, String.class);
        //获取返回数据
        return resp.getBody();
    }
}