package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysToolsMapper;
import com.ruoyi.system.domain.SysTools;
import com.ruoyi.system.service.ISysToolsService;

/**
 * 工具清单管理Service业务层处理
 * 
 * @author zuolei
 * @date 2022-08-08
 */
@Service
public class SysToolsServiceImpl implements ISysToolsService 
{
    @Autowired
    private SysToolsMapper sysToolsMapper;

    /**
     * 查询工具清单管理
     * 
     * @param toolsId 工具清单管理主键
     * @return 工具清单管理
     */
    @Override
    public SysTools selectSysToolsByToolsId(Long toolsId)
    {
        return sysToolsMapper.selectSysToolsByToolsId(toolsId);
    }

    /**
     * 查询工具清单管理列表
     * 
     * @param sysTools 工具清单管理
     * @return 工具清单管理
     */
    @Override
    public List<SysTools> selectSysToolsList(SysTools sysTools)
    {
        return sysToolsMapper.selectSysToolsList(sysTools);
    }

    /**
     * 新增工具清单管理
     * 
     * @param sysTools 工具清单管理
     * @return 结果
     */
    @Override
    public int insertSysTools(SysTools sysTools)
    {
        sysTools.setCreateTime(DateUtils.getNowDate());
        return sysToolsMapper.insertSysTools(sysTools);
    }

    /**
     * 修改工具清单管理
     * 
     * @param sysTools 工具清单管理
     * @return 结果
     */
    @Override
    public int updateSysTools(SysTools sysTools)
    {
        sysTools.setUpdateTime(DateUtils.getNowDate());
        return sysToolsMapper.updateSysTools(sysTools);
    }

    /**
     * 批量删除工具清单管理
     * 
     * @param toolsIds 需要删除的工具清单管理主键
     * @return 结果
     */
    @Override
    public int deleteSysToolsByToolsIds(Long[] toolsIds)
    {
        return sysToolsMapper.deleteSysToolsByToolsIds(toolsIds);
    }

    /**
     * 删除工具清单管理信息
     * 
     * @param toolsId 工具清单管理主键
     * @return 结果
     */
    @Override
    public int deleteSysToolsByToolsId(Long toolsId)
    {
        return sysToolsMapper.deleteSysToolsByToolsId(toolsId);
    }
}
