package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysToolsDetailMapper;
import com.ruoyi.system.domain.SysToolsDetail;
import com.ruoyi.system.service.ISysToolsDetailService;

/**
 * 工具管理Service业务层处理
 * 
 * @author zuolei
 * @date 2022-08-08
 */
@Service
public class SysToolsDetailServiceImpl implements ISysToolsDetailService 
{
    @Autowired
    private SysToolsDetailMapper sysToolsDetailMapper;

    /**
     * 查询工具管理
     * 
     * @param toolId 工具管理主键
     * @return 工具管理
     */
    @Override
    public SysToolsDetail selectSysToolsDetailByToolId(Long toolId)
    {
        return sysToolsDetailMapper.selectSysToolsDetailByToolId(toolId);
    }

    /**
     * 查询工具管理列表
     * 
     * @param sysToolsDetail 工具管理
     * @return 工具管理
     */
    @Override
    public List<SysToolsDetail> selectSysToolsDetailList(SysToolsDetail sysToolsDetail)
    {
        return sysToolsDetailMapper.selectSysToolsDetailList(sysToolsDetail);
    }

    /**
     * 新增工具管理
     * 
     * @param sysToolsDetail 工具管理
     * @return 结果
     */
    @Override
    public int insertSysToolsDetail(SysToolsDetail sysToolsDetail)
    {
        sysToolsDetail.setCreateTime(DateUtils.getNowDate());
        return sysToolsDetailMapper.insertSysToolsDetail(sysToolsDetail);
    }

    /**
     * 修改工具管理
     * 
     * @param sysToolsDetail 工具管理
     * @return 结果
     */
    @Override
    public int updateSysToolsDetail(SysToolsDetail sysToolsDetail)
    {
        sysToolsDetail.setUpdateTime(DateUtils.getNowDate());
        return sysToolsDetailMapper.updateSysToolsDetail(sysToolsDetail);
    }

    /**
     * 批量删除工具管理
     * 
     * @param toolIds 需要删除的工具管理主键
     * @return 结果
     */
    @Override
    public int deleteSysToolsDetailByToolIds(Long[] toolIds)
    {
        return sysToolsDetailMapper.deleteSysToolsDetailByToolIds(toolIds);
    }

    /**
     * 删除工具管理信息
     * 
     * @param toolId 工具管理主键
     * @return 结果
     */
    @Override
    public int deleteSysToolsDetailByToolId(Long toolId)
    {
        return sysToolsDetailMapper.deleteSysToolsDetailByToolId(toolId);
    }
}
