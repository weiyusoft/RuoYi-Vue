package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysPartCategoryMapper;
import com.ruoyi.common.core.domain.entity.SysPartCategory;
import com.ruoyi.system.service.ISysPartCategoryService;

/**
 * 配件分类Service业务层处理
 *
 * @author zuolei
 * @date 2022-07-08
 */
@Service
public class SysPartCategoryServiceImpl implements ISysPartCategoryService
{

    @Autowired
    private SysPartCategoryMapper sysPartCategoryMapper;

    /**
     * 查询配件分类
     *
     * @param categoryId 配件分类主键
     * @return 配件分类
     */
    @Override
    public SysPartCategory selectSysPartCategoryByCategoryId(Long categoryId)
    {
        return sysPartCategoryMapper.selectSysPartCategoryByCategoryId(categoryId);
    }

    /**
     * 查询配件分类
     *
     * @param name 配件分类中文或英文名称
     * @return 配件分类
     */
    @Override
    public SysPartCategory selectSysPartCategoryByName(String name)
    {
        return sysPartCategoryMapper.selectSysPartCategoryByName(name);
    }

    /**
     * 查询配件分类列表
     *
     * @param sysPartCategory 配件分类
     * @return 配件分类
     */
    @Override
    public List<SysPartCategory> selectSysPartCategoryList(SysPartCategory sysPartCategory)
    {
        return sysPartCategoryMapper.selectSysPartCategoryList(sysPartCategory);
    }

    /**
     * 构建前端所需要树结构
     *
     * @param categories 分类列表
     * @return 树结构列表
     */
    @Override
    public List<SysPartCategory> buildCategoryTree(List<SysPartCategory> categories)
    {
        List<SysPartCategory> returnList = new ArrayList<SysPartCategory>();
        List<Long> tempList = new ArrayList<Long>();
        for (SysPartCategory category : categories)
        {
            tempList.add(category.getCategoryId());
        }
        for (SysPartCategory category : categories)
        {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(category.getParentId()))
            {
                recursionFn(categories, category);
                returnList.add(category);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = categories;
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<SysPartCategory> list, SysPartCategory t)
    {
        // 得到子节点列表
        List<SysPartCategory> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysPartCategory tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysPartCategory> list, SysPartCategory t)
    {
        return getChildList(list, t).size() > 0;
    }


    /**
     * 得到子节点列表
     */
    private List<SysPartCategory> getChildList(List<SysPartCategory> list, SysPartCategory t)
    {
        List<SysPartCategory> tlist = new ArrayList<>();
        Iterator<SysPartCategory> it = list.iterator();
        while (it.hasNext())
        {
            SysPartCategory n = (SysPartCategory) it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getCategoryId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }
    /**
     * 构建前端所需要下拉树结构
     *
     * @param categories 分类列表
     * @return 下拉树结构列表
     */
    @Override
    public List<TreeSelect> buildCategoryTreeSelect(List<SysPartCategory> categories)
    {
        List<SysPartCategory> treeCategories = buildCategoryTree(categories);
        return treeCategories.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 新增配件分类
     *
     * @param sysPartCategory 配件分类
     * @return 结果
     */
    @Override
    public int insertSysPartCategory(SysPartCategory sysPartCategory)
    {
        SysPartCategory parentCategory = null;
        if (sysPartCategory.getParentId() != 0)
        {
            parentCategory = selectSysPartCategoryByCategoryId(sysPartCategory.getParentId());
            if (parentCategory == null)
            {
                throw new ServiceException("父分类不存在，不允许新增");
            }
            if (UserConstants.DEL_FLAG_DELETE.equals(parentCategory.getDelFlag()))
            {
                throw new ServiceException("父分类已停用，不允许新增");
            }
            sysPartCategory.setAncestors(parentCategory.getAncestors() + "," + sysPartCategory.getParentId());
        } else
        {
            sysPartCategory.setAncestors("0");
        }
        sysPartCategory.setCreateTime(DateUtils.getNowDate());

        return sysPartCategoryMapper.insertSysPartCategory(sysPartCategory);
    }

    /**
     * 修改配件分类
     *
     * @param sysPartCategory 配件分类
     * @return 结果
     */
    @Override
    public int updateSysPartCategory(SysPartCategory sysPartCategory)
    {
        sysPartCategory.setUpdateTime(DateUtils.getNowDate());
        return sysPartCategoryMapper.updateSysPartCategory(sysPartCategory);
    }

    /**
     * 批量删除配件分类
     *
     * @param categoryIds 需要删除的配件分类主键
     * @return 结果
     */
    @Override
    public int deleteSysPartCategoryByCategoryIds(Long[] categoryIds)
    {
        return sysPartCategoryMapper.deleteSysPartCategoryByCategoryIds(categoryIds);
    }

    /**
     * 删除配件分类信息
     *
     * @param categoryId 配件分类主键
     * @return 结果
     */
    @Override
    public int deleteSysPartCategoryByCategoryId(Long categoryId)
    {
        return sysPartCategoryMapper.deleteSysPartCategoryByCategoryId(categoryId);
    }
}
