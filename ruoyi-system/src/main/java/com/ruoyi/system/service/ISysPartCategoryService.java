package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.TreeSelect;
import java.util.List;
import com.ruoyi.common.core.domain.entity.SysPartCategory;

/**
 * 配件分类Service接口
 *
 * @author zuolei
 * @date 2022-07-08
 */
public interface ISysPartCategoryService
{

    /**
     * 查询配件分类
     *
     * @param categoryId 配件分类主键
     * @return 配件分类
     */
    public SysPartCategory selectSysPartCategoryByCategoryId(Long categoryId);

    /**
     * 查询配件分类
     *
     * @param name 配件分类中文名称或英文名称
     * @return 配件分类
     */
    public SysPartCategory selectSysPartCategoryByName(String name);

    /**
     * 查询配件分类列表
     *
     * @param sysPartCategory 配件分类
     * @return 配件分类集合
     */
    public List<SysPartCategory> selectSysPartCategoryList(SysPartCategory sysPartCategory);


    /**
     * 构建前端所需要树结构
     *
     * @param categories 分类列表
     * @return 树结构列表
     */
    public List<SysPartCategory> buildCategoryTree(List<SysPartCategory> categories);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param categories 分类列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildCategoryTreeSelect(List<SysPartCategory> categories);

    /**
     * 新增配件分类
     *
     * @param sysPartCategory 配件分类
     * @return 结果
     */
    public int insertSysPartCategory(SysPartCategory sysPartCategory);

    /**
     * 修改配件分类
     *
     * @param sysPartCategory 配件分类
     * @return 结果
     */
    public int updateSysPartCategory(SysPartCategory sysPartCategory);

    /**
     * 批量删除配件分类
     *
     * @param categoryIds 需要删除的配件分类主键集合
     * @return 结果
     */
    public int deleteSysPartCategoryByCategoryIds(Long[] categoryIds);

    /**
     * 删除配件分类信息
     *
     * @param categoryId 配件分类主键
     * @return 结果
     */
    public int deleteSysPartCategoryByCategoryId(Long categoryId);
}
