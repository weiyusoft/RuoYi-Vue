package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysProduct;

/**
 * 产品/报价单Service接口
 *
 * @author zuolei
 * @date 2022-07-25
 */
public interface ISysProductService
{

    /**
     * 查询产品/报价单
     *
     * @param productId 产品/报价单主键
     * @return 产品/报价单
     */
    public SysProduct selectSysProductByProductId(Long productId);

    /**
     * 查询产品/报价单列表
     *
     * @param sysProduct 产品/报价单
     * @return 产品/报价单集合
     */
    public List<SysProduct> selectSysProductList(SysProduct sysProduct);

    /**
     * 新增产品/报价单
     *
     * @param sysProduct 产品/报价单
     * @return 结果
     */
    public int insertSysProduct(SysProduct sysProduct);

    /**
     * 查询产品/报价单
     *
     * @param name 名称
     * @return 产品/报价单集合
     */
    public SysProduct selectSysProductByName(String name);

    /**
     * 修改产品/报价单
     *
     * @param sysProduct 产品/报价单
     * @return 结果
     */
    public int updateSysProduct(SysProduct sysProduct);

    /**
     * 批量删除产品/报价单
     *
     * @param productIds 需要删除的产品/报价单主键集合
     * @return 结果
     */
    public int deleteSysProductByProductIds(Long[] productIds);

    /**
     * 删除产品/报价单信息
     *
     * @param productId 产品/报价单主键
     * @return 结果
     */
    public int deleteSysProductByProductId(Long productId);
}
