package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysPart;

/**
 * 配件Service接口
 *
 * @author ruoyi
 * @date 2022-07-08
 */
public interface ISysPartService
{

    /**
     * 查询配件
     *
     * @param partId 配件主键
     * @return 配件
     */
    public SysPart selectSysPartByPartId(Long partId);

    /**
     * 查询配件
     *
     * @param name 配件名称
     * @return 配件
     */
    public SysPart selectSysPartByName(String name);

    /**
     * 查询配件列表
     *
     * @param sysPart 配件
     * @return 配件集合
     */
    public List<SysPart> selectSysPartList(SysPart sysPart);

    /**
     * 新增配件
     *
     * @param sysPart 配件
     * @return 结果
     */
    public int insertSysPart(SysPart sysPart);

    /**
     * 修改配件
     *
     * @param sysPart 配件
     * @return 结果
     */
    public int updateSysPart(SysPart sysPart);

    /**
     * 批量删除配件
     *
     * @param partIds 需要删除的配件主键集合
     * @return 结果
     */
    public int deleteSysPartByPartIds(Long[] partIds);

    /**
     * 删除配件信息
     *
     * @param partId 配件主键
     * @return 结果
     */
    public int deleteSysPartByPartId(Long partId);
}
