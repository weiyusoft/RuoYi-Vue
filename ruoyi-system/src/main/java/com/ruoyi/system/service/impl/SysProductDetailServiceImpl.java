package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysProductDetailMapper;
import com.ruoyi.system.domain.SysProductDetail;
import com.ruoyi.system.service.ISysProductDetailService;

/**
 * 产品/报价单关联详情Service业务层处理
 *
 * @author zuolei
 * @date 2022-07-25
 */
@Service
public class SysProductDetailServiceImpl implements ISysProductDetailService
{

    @Autowired
    private SysProductDetailMapper sysProductDetailMapper;

    /**
     * 查询产品/报价单关联详情
     *
     * @param productDetailId 产品/报价单关联详情主键
     * @return 产品/报价单关联详情
     */
    @Override
    public SysProductDetail selectSysProductDetailByProductDetailId(Long productDetailId)
    {
        return sysProductDetailMapper.selectSysProductDetailByProductDetailId(productDetailId);
    }

    /**
     * 查询产品/报价单关联详情列表
     *
     * @param sysProductDetail 产品/报价单关联详情
     * @return 产品/报价单关联详情
     */
    @Override
    public List<SysProductDetail> selectSysProductDetailList(SysProductDetail sysProductDetail)
    {
        return sysProductDetailMapper.selectSysProductDetailList(sysProductDetail);
    }

    /**
     * 新增产品/报价单关联详情
     *
     * @param sysProductDetail 产品/报价单关联详情
     * @return 结果
     */
    @Override
    public int insertSysProductDetail(SysProductDetail sysProductDetail)
    {
        sysProductDetail.setCreateTime(DateUtils.getNowDate());
        return sysProductDetailMapper.insertSysProductDetail(sysProductDetail);
    }

    /**
     * 修改产品/报价单关联详情
     *
     * @param sysProductDetail 产品/报价单关联详情
     * @return 结果
     */
    @Override
    public int updateSysProductDetail(SysProductDetail sysProductDetail)
    {
        sysProductDetail.setUpdateTime(DateUtils.getNowDate());
        return sysProductDetailMapper.updateSysProductDetail(sysProductDetail);
    }

    /**
     * 批量删除产品/报价单关联详情
     *
     * @param productDetailIds 需要删除的产品/报价单关联详情主键
     * @return 结果
     */
    @Override
    public int deleteSysProductDetailByProductDetailIds(Long[] productDetailIds)
    {
        return sysProductDetailMapper.deleteSysProductDetailByProductDetailIds(productDetailIds);
    }

    /**
     * 删除产品/报价单关联详情信息
     *
     * @param productId 产品/报价单关联详情主键
     * @return 结果
     */
    @Override
    public int deleteSysProductDetailByProductId(Long productId)
    {
        return sysProductDetailMapper.deleteSysProductDetailByProductId(productId);
    }
}
