package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.SysPartDetail;
import com.ruoyi.system.mapper.SysPartDetailMapper;
import java.util.ArrayList;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysPartMapper;
import com.ruoyi.system.domain.SysPart;
import com.ruoyi.system.service.ISysPartService;

/**
 * 配件Service业务层处理
 *
 * @author ruoyi
 * @date 2022-07-08
 */
@Service
public class SysPartServiceImpl implements ISysPartService
{

    @Autowired
    private SysPartMapper sysPartMapper;

    /**
     * 查询配件
     *
     * @param partId 配件主键
     * @return 配件
     */
    @Override
    public SysPart selectSysPartByPartId(Long partId)
    {
        return sysPartMapper.selectSysPartByPartId(partId);
    }

    /**
     * 查询配件
     *
     * @param name 配件名称
     * @return 配件
     */
    @Override
    public SysPart selectSysPartByName(String name)
    {
        return sysPartMapper.selectSysPartByName(name);
    }

    /**
     * 查询配件列表
     *
     * @param sysPart 配件
     * @return 配件
     */
    @Override
    public List<SysPart> selectSysPartList(SysPart sysPart)
    {
        return sysPartMapper.selectSysPartList(sysPart);
    }


    /**
     * 新增配件
     *
     * @param sysPart 配件
     * @return 结果
     */
    @Override
    public int insertSysPart(SysPart sysPart)
    {
        sysPart.setCreateTime(DateUtils.getNowDate());
        return sysPartMapper.insertSysPart(sysPart);
    }

    /**
     * 修改配件
     *
     * @param sysPart 配件
     * @return 结果
     */
    @Override
    public int updateSysPart(SysPart sysPart)
    {
        sysPart.setUpdateTime(DateUtils.getNowDate());
        return sysPartMapper.updateSysPart(sysPart);
    }

    /**
     * 批量删除配件
     *
     * @param partIds 需要删除的配件主键
     * @return 结果
     */
    @Override
    public int deleteSysPartByPartIds(Long[] partIds)
    {
        return sysPartMapper.deleteSysPartByPartIds(partIds);
    }

    /**
     * 删除配件信息
     *
     * @param partId 配件主键
     * @return 结果
     */
    @Override
    public int deleteSysPartByPartId(Long partId)
    {
        return sysPartMapper.deleteSysPartByPartId(partId);
    }
}
