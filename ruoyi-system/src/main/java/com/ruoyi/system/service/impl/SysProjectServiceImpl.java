package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.system.domain.SysToolsDetail;
import com.ruoyi.system.domain.vo.ProjectStatusVo;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.ruoyi.system.mapper.SysToolsDetailMapper;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysProjectMapper;
import com.ruoyi.system.domain.SysProject;
import com.ruoyi.system.service.ISysProjectService;

/**
 * 项目管理Service业务层处理
 *
 * @author zuolei
 * @date 2022-07-30
 */
@Service
public class SysProjectServiceImpl implements ISysProjectService
{

    @Autowired
    private SysProjectMapper sysProjectMapper;

    @Autowired
    private SysDictDataMapper sysDictDataMapper;

    @Autowired
    private SysToolsDetailMapper sysToolsDetailMapper;

    /**
     * 查询项目管理
     *
     * @param projectId 项目管理主键
     * @return 项目管理
     */
    @Override
    public SysProject selectSysProjectByProjectId(Long projectId)
    {
        SysProject sysProject = sysProjectMapper.selectSysProjectByProjectId(projectId);
        Long standardId = sysProject.getStandard();
        if (standardId != null && standardId != 0)
        {
            SysDictData standardData = sysDictDataMapper.selectDictDataById(standardId);
            if (standardData != null)
            {
                sysProject.setStandardKey(standardData.getDictLabel());
                sysProject.setStandardValue(standardData.getDictValue());
            }
        }
//        String scopeId = sysProject.getApplicationscope();
//        if (scopeId != null && scopeId != 0)
//        {
//            SysDictData scopeData = sysDictDataMapper.selectDictDataById(scopeId);
//            if (scopeData != null)
//            {
//                sysProject.setApplicationscopeKey(scopeData.getDictLabel());
//                sysProject.setApplicationscopeValue(scopeData.getDictValue());
//            }
//        }

        Long conditionId = sysProject.getConditions();
        if (conditionId != null && conditionId != 0)
        {
            SysDictData conditionData = sysDictDataMapper.selectDictDataById(conditionId);
            if (conditionData != null)
            {
                sysProject.setConditionsKey(conditionData.getDictLabel());
                sysProject.setConditionsValue(conditionData.getDictValue());
            }
        }

//        Long abilityId = sysProject.getAbility();
//        if (abilityId != null && abilityId != 0)
//        {
//            SysDictData abilityData = sysDictDataMapper.selectDictDataById(abilityId);
//            if (abilityData != null)
//            {
//                sysProject.setAbilityKey(abilityData.getDictLabel());
//                sysProject.setAbilityValue(abilityData.getDictValue());
//            }
//        }

        Long layoutId = sysProject.getLayout();
        if (layoutId != null && layoutId != 0)
        {
            SysDictData layoutData = sysDictDataMapper.selectDictDataById(layoutId);
            if (layoutData != null)
            {
                sysProject.setLayoutKey(layoutData.getDictLabel());
                sysProject.setLayoutValue(layoutData.getDictValue());
            }
        }

        Long toolsId = sysProject.getTools();
        if (toolsId != null && toolsId != 0)
        {
            SysToolsDetail sysToolsDetail = new SysToolsDetail();
            if (sysToolsDetail != null)
            {
                sysToolsDetail.setParentId(toolsId);
                List<SysToolsDetail> sysToolsDetails = sysToolsDetailMapper.selectSysToolsDetailList(sysToolsDetail);
                sysProject.setToolsDetails(sysToolsDetails);
            }
        }
        return sysProject;
    }

    /**
     * 查询项目管理列表
     *
     * @param sysProject 项目管理
     * @return 项目管理
     */
    @Override
    public List<SysProject> selectSysProjectList(SysProject sysProject)
    {
        return sysProjectMapper.selectSysProjectList(sysProject);
    }

    /**
     * 新增项目管理
     *
     * @param sysProject 项目管理
     * @return 结果
     */
    @Override
    public int insertSysProject(SysProject sysProject)
    {
        sysProject.setCreateTime(DateUtils.getNowDate());
        return sysProjectMapper.insertSysProject(sysProject);
    }

    /**
     * 修改项目管理
     *
     * @param sysProject 项目管理
     * @return 结果
     */
    @Override
    public int updateSysProject(SysProject sysProject)
    {
        sysProject.setUpdateTime(DateUtils.getNowDate());
        return sysProjectMapper.updateSysProject(sysProject);
    }

    /**
     * 批量删除项目管理
     *
     * @param projectIds 需要删除的项目管理主键
     * @return 结果
     */
    @Override
    public int deleteSysProjectByProjectIds(Long[] projectIds)
    {
        return sysProjectMapper.deleteSysProjectByProjectIds(projectIds);
    }

    /**
     * 删除项目管理信息
     *
     * @param projectId 项目管理主键
     * @return 结果
     */
    @Override
    public int deleteSysProjectByProjectId(Long projectId)
    {
        return sysProjectMapper.deleteSysProjectByProjectId(projectId);
    }

    /**
     * 查询项目列表
     *
     * @param userIds 用户id集合
     * @return 结果
     */
    @Override
    public List<SysProject> selectSysProjectByUserId(Long[] userIds)
    {
        return sysProjectMapper.selectSysProjectByUserId(userIds);
    }

    @Override
    public int updateProjectStatus(ProjectStatusVo statusVo)
    {
        return sysProjectMapper.updateProjectStatus(statusVo);
    }
}
