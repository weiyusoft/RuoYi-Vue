package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysPartDetailMapper;
import com.ruoyi.system.domain.SysPartDetail;
import com.ruoyi.system.service.ISysPartDetailService;

/**
 * 配件详情Service业务层处理
 *
 * @author zuolei
 * @date 2022-07-09
 */
@Service
public class SysPartDetailServiceImpl implements ISysPartDetailService
{

    @Autowired
    private SysPartDetailMapper sysPartDetailMapper;

    /**
     * 查询配件详情
     *
     * @param partDetailId 配件详情主键
     * @return 配件详情
     */
    @Override
    public SysPartDetail selectSysPartDetailByPartDetailId(Long partDetailId)
    {
        return sysPartDetailMapper.selectSysPartDetailByPartDetailId(partDetailId);
    }

    /**
     * 查询配件详情
     *
     * @param subPartId 易损件id
     * @return 配件详情
     */
    @Override
    public SysPartDetail selectSysPartDetailBySubPartId(Long subPartId)
    {
        return sysPartDetailMapper.selectSysPartDetailBySubPartId(subPartId);
    }

    /**
     * 查询配件详情列表
     *
     * @param sysPartDetail 配件详情
     * @return 配件详情
     */
    @Override
    public List<SysPartDetail> selectSysPartDetailList(SysPartDetail sysPartDetail)
    {
        return sysPartDetailMapper.selectSysPartDetailList(sysPartDetail);
    }

    /**
     * 新增配件详情
     *
     * @param sysPartDetail 配件详情
     * @return 结果
     */
    @Override
    public int insertSysPartDetail(SysPartDetail sysPartDetail)
    {
        sysPartDetail.setCreateTime(DateUtils.getNowDate());
        return sysPartDetailMapper.insertSysPartDetail(sysPartDetail);
    }

    /**
     * 修改配件详情
     *
     * @param sysPartDetail 配件详情
     * @return 结果
     */
    @Override
    public int updateSysPartDetail(SysPartDetail sysPartDetail)
    {
        sysPartDetail.setUpdateTime(DateUtils.getNowDate());
        return sysPartDetailMapper.updateSysPartDetail(sysPartDetail);
    }

    /**
     * 批量删除配件详情
     *
     * @param partDetailIds 需要删除的配件详情主键
     * @return 结果
     */
    @Override
    public int deleteSysPartDetailByPartDetailIds(Long[] partDetailIds)
    {
        return sysPartDetailMapper.deleteSysPartDetailByPartDetailIds(partDetailIds);
    }

    /**
     * 删除配件详情信息
     *
     * @param partDetailId 配件详情主键
     * @return 结果
     */
    @Override
    public int deleteSysPartDetailByPartDetailId(Long partDetailId)
    {
        return sysPartDetailMapper.deleteSysPartDetailByPartDetailId(partDetailId);
    }

    /**
     * 批量删除配件详情
     *
     * @param partIds 需要删除的partId集合
     * @return 结果
     */
    @Override
    public int deleteSysPartDetailByPartIds(Long[] partIds)
    {
        return sysPartDetailMapper.deleteSysPartDetailByPartIds(partIds);
    }
}
