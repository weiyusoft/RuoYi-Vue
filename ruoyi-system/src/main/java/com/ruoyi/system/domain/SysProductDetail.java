package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品/报价单关联详情对象 sys_product_detail
 * 
 * @author zuolei
 * @date 2022-07-25
 */
public class SysProductDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 详情id */
    private Long productDetailId;

    /** 产品id */
    @Excel(name = "产品id")
    private Long productId;

    /** 产品编号 */
    @Excel(name = "产品编号")
    private String productNo;

    /** 产品总价 */
    @Excel(name = "产品总价")
    private BigDecimal price;

    /** 配件id */
    @Excel(name = "配件id")
    private Long partId;

    /** 配件数量 */
    @Excel(name = "配件数量")
    private Long partCount;

    /** 类型（1-配件，2-易损件，3-自定义配件）  */
    @Excel(name = "类型", readConverterExp = "1=-配件，2-易损件，3-自定义配件")
    private Integer partType;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setProductDetailId(Long productDetailId) 
    {
        this.productDetailId = productDetailId;
    }

    public Long getProductDetailId() 
    {
        return productDetailId;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setProductNo(String productNo) 
    {
        this.productNo = productNo;
    }

    public String getProductNo() 
    {
        return productNo;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setPartId(Long partId) 
    {
        this.partId = partId;
    }

    public Long getPartId() 
    {
        return partId;
    }
    public void setPartCount(Long partCount) 
    {
        this.partCount = partCount;
    }

    public Long getPartCount() 
    {
        return partCount;
    }
    public void setPartType(Integer partType) 
    {
        this.partType = partType;
    }

    public Integer getPartType() 
    {
        return partType;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("productDetailId", getProductDetailId())
            .append("productId", getProductId())
            .append("productNo", getProductNo())
            .append("price", getPrice())
            .append("partId", getPartId())
            .append("partCount", getPartCount())
            .append("partType", getPartType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
