package com.ruoyi.system.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 项目管理对象 sys_project
 *
 * @author zuolei
 * @date 2022-08-08
 */
public class SysProject extends BaseEntity
{

    private static final long serialVersionUID = 1L;

    /**
     * 项目id
     */
    private Long projectId;

    /**
     * 项目编号
     */
    @Excel(name = "项目编号")
    private String projectNo;

    /**
     * 中文名称
     */
    @Excel(name = "中文名称")
    private String name;

    /**
     * 英文名称
     */
    @Excel(name = "英文名称")
    private String englishName;

    /**
     * 中文描述
     */
    @Excel(name = "中文描述")
    private String description;

    /**
     * 英文描述
     */
    @Excel(name = "英文描述")
    private String englishDescription;

    /**
     * 合作公司
     */
    @Excel(name = "合作公司")
    private String partnerCompany;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String contact;

    /**
     * 联系方式
     */
    @Excel(name = "联系方式")
    private String contactPhone;

    /**
     * 管理员id
     */
    @Excel(name = "管理员id")
    private Long manager;

    /**
     * 管理员姓名
     */
    @Excel(name = "管理员姓名")
    private String managerName;

    /**
     * 拥有者id
     */
    @Excel(name = "拥有者id")
    private Long owner;

    /**
     * 状态: 1-进行中 2-已中标 3-未中标
     */
    @Excel(name = "状态: 1-进行中 2-已中标 3-未中标")
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 设备标准
     */
    @Excel(name = "设备标准")
    private Long standard;

    private String standardKey;

    private String standardValue;

    /**
     * 适用范围
     */
    @Excel(name = "适用范围")
    private String applicationscope;

    private String applicationscopeKey;

    private String applicationscopeValue;

    /**
     * 适用条件
     */
    @Excel(name = "适用条件")
    private Long conditions;

    private String conditionsKey;

    private String conditionsValue;

    /**
     * 设备能力
     */
    @Excel(name = "设备能力")
    private String ability;

    private String abilityKey;

    private String abilityValue;

    /**
     * 系统布局
     */
    @Excel(name = "系统布局")
    private Long layout;

    private String layoutKey;

    private String layoutValue;

    /**
     * 工具清单
     */
    @Excel(name = "工具清单")
    private Long tools;

    private List<SysToolsDetail>  toolsDetails;

    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    public Long getProjectId()
    {
        return projectId;
    }

    public void setProjectNo(String projectNo)
    {
        this.projectNo = projectNo;
    }

    public String getProjectNo()
    {
        return projectNo;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setEnglishName(String englishName)
    {
        this.englishName = englishName;
    }

    public String getEnglishName()
    {
        return englishName;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public void setEnglishDescription(String englishDescription)
    {
        this.englishDescription = englishDescription;
    }

    public String getEnglishDescription()
    {
        return englishDescription;
    }

    public void setPartnerCompany(String partnerCompany)
    {
        this.partnerCompany = partnerCompany;
    }

    public String getPartnerCompany()
    {
        return partnerCompany;
    }

    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public String getContact()
    {
        return contact;
    }

    public void setContactPhone(String contactPhone)
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone()
    {
        return contactPhone;
    }

    public void setManager(Long manager)
    {
        this.manager = manager;
    }

    public Long getManager()
    {
        return manager;
    }

    public void setManagerName(String managerName)
    {
        this.managerName = managerName;
    }

    public String getManagerName()
    {
        return managerName;
    }

    public void setOwner(Long owner)
    {
        this.owner = owner;
    }

    public Long getOwner()
    {
        return owner;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public void setStandard(Long standard)
    {
        this.standard = standard;
    }

    public Long getStandard()
    {
        return standard;
    }

    public void setApplicationscope(String applicationscope)
    {
        this.applicationscope = applicationscope;
    }

    public String getApplicationscope()
    {
        return applicationscope;
    }

    public void setConditions(Long conditions)
    {
        this.conditions = conditions;
    }

    public Long getConditions()
    {
        return conditions;
    }

    public void setAbility(String ability)
    {
        this.ability = ability;
    }

    public String getAbility()
    {
        return ability;
    }

    public void setLayout(Long layout)
    {
        this.layout = layout;
    }

    public Long getLayout()
    {
        return layout;
    }

    public void setTools(Long tools)
    {
        this.tools = tools;
    }

    public Long getTools()
    {
        return tools;
    }

    public String getStandardKey()
    {
        return standardKey;
    }

    public void setStandardKey(String standardKey)
    {
        this.standardKey = standardKey;
    }

    public String getStandardValue()
    {
        return standardValue;
    }

    public void setStandardValue(String standardValue)
    {
        this.standardValue = standardValue;
    }

    public String getApplicationscopeKey()
    {
        return applicationscopeKey;
    }

    public void setApplicationscopeKey(String applicationscopeKey)
    {
        this.applicationscopeKey = applicationscopeKey;
    }

    public String getApplicationscopeValue()
    {
        return applicationscopeValue;
    }

    public void setApplicationscopeValue(String applicationscopeValue)
    {
        this.applicationscopeValue = applicationscopeValue;
    }

    public String getConditionsKey()
    {
        return conditionsKey;
    }

    public void setConditionsKey(String conditionsKey)
    {
        this.conditionsKey = conditionsKey;
    }

    public String getConditionsValue()
    {
        return conditionsValue;
    }

    public void setConditionsValue(String conditionsValue)
    {
        this.conditionsValue = conditionsValue;
    }

    public String getAbilityKey()
    {
        return abilityKey;
    }

    public void setAbilityKey(String abilityKey)
    {
        this.abilityKey = abilityKey;
    }

    public String getAbilityValue()
    {
        return abilityValue;
    }

    public void setAbilityValue(String abilityValue)
    {
        this.abilityValue = abilityValue;
    }

    public String getLayoutKey()
    {
        return layoutKey;
    }

    public void setLayoutKey(String layoutKey)
    {
        this.layoutKey = layoutKey;
    }

    public String getLayoutValue()
    {
        return layoutValue;
    }

    public void setLayoutValue(String layoutValue)
    {
        this.layoutValue = layoutValue;
    }

    public List<SysToolsDetail> getToolsDetails()
    {
        return toolsDetails;
    }

    public void setToolsDetails(List<SysToolsDetail> toolsDetails)
    {
        this.toolsDetails = toolsDetails;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("projectId", getProjectId())
            .append("projectNo", getProjectNo())
            .append("name", getName())
            .append("englishName", getEnglishName())
            .append("description", getDescription())
            .append("englishDescription", getEnglishDescription())
            .append("partnerCompany", getPartnerCompany())
            .append("contact", getContact())
            .append("contactPhone", getContactPhone())
            .append("manager", getManager())
            .append("managerName", getManagerName())
            .append("owner", getOwner())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("standard", getStandard())
            .append("applicationscope", getApplicationscope())
            .append("conditions", getConditions())
            .append("ability", getAbility())
            .append("layout", getLayout())
            .append("tools", getTools())
            .toString();
    }
}
