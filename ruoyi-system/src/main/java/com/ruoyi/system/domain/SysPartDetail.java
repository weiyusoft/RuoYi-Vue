package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 配件详情对象 sys_part_detail
 *
 * @author zuolei
 * @date 2022-07-09
 */
public class SysPartDetail extends BaseEntity
{

    private static final long serialVersionUID = 1L;

    private String name;
    /**
     * 配件详情id
     */
    private Long partDetailId;

    /**
     * 配件id
     */
    @Excel(name = "配件id")
    private Long partId;

    /**
     * 关联配件id
     */
    @Excel(name = "关联配件id")
    private Long subPartId;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    private SysPart part;

    public void setPartDetailId(Long partDetailId)
    {
        this.partDetailId = partDetailId;
    }

    public Long getPartDetailId()
    {
        return partDetailId;
    }

    public void setPartId(Long partId)
    {
        this.partId = partId;
    }

    public Long getPartId()
    {
        return partId;
    }

    public void setSubPartId(Long subPartId)
    {
        this.subPartId = subPartId;
    }

    public Long getSubPartId()
    {
        return subPartId;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public SysPart getPart()
    {
        return part;
    }

    public void setPart(SysPart part)
    {
        this.part = part;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("partDetailId", getPartDetailId())
            .append("partId", getPartId())
            .append("subPartId", getSubPartId())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
