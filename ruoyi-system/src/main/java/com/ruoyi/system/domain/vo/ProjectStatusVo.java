package com.ruoyi.system.domain.vo;

import java.util.Arrays;

public class ProjectStatusVo
{

    private String status;

    private String[] projectIds;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String[] getProjectIds()
    {
        return projectIds;
    }

    public void setProjectIds(String[] projectIds)
    {
        this.projectIds = projectIds;
    }

    @Override
    public String toString()
    {
        return "ProjectStatusVo{" +
            "status='" + status + '\'' +
            ", projectIds=" + Arrays.toString(projectIds) +
            '}';
    }
}
