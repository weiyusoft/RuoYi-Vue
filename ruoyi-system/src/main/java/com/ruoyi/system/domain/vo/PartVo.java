package com.ruoyi.system.domain.vo;

/**
 * 配件对象 sys_part
 *
 * @author zuolei
 * @date 2022-07-12
 */
public class PartVo
{

    private Long partId;
    private String name;
    private String englishName;
    private Integer type;
    private Long count;
    private Long parentPartId;
    private String manufacturer;
    /**
     * 是否可配置易损件(0-可配置 2-不可配置)
     **/
    private String subPartEnable;

    public Long getPartId()
    {
        return partId;
    }

    public void setPartId(Long partId)
    {
        this.partId = partId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEnglishName()
    {
        return englishName;
    }

    public void setEnglishName(String englishName)
    {
        this.englishName = englishName;
    }

    public Integer getType()
    {
        return type;
    }

    public void setType(Integer type)
    {
        this.type = type;
    }

    public Long getCount()
    {
        return count;
    }

    public void setCount(Long count)
    {
        this.count = count;
    }

    public String getSubPartEnable()
    {
        return subPartEnable;
    }

    public void setSubPartEnable(String subPartEnable)
    {
        this.subPartEnable = subPartEnable;
    }

    public Long getParentPartId()
    {
        return parentPartId;
    }

    public void setParentPartId(Long parentPartId)
    {
        this.parentPartId = parentPartId;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString()
    {
        return "PartVo{" +
            "partId=" + partId +
            ", name='" + name + '\'' +
            ", englishName='" + englishName + '\'' +
            ", type=" + type +
            ", count=" + count +
            ", parentPartId=" + parentPartId +
            ", manufacturer='" + manufacturer + '\'' +
            ", subPartEnable='" + subPartEnable + '\'' +
            '}';
    }
}
