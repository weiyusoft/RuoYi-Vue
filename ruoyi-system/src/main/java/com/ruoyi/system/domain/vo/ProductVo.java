package com.ruoyi.system.domain.vo;

import java.math.BigDecimal;

public class ProductVo
{

    private Long productId;

    private Long partCount;

    private Long consumablePartCount;

    private Long customPartCount;

    private BigDecimal price;

    public Long getProductId()
    {
        return productId;
    }

    public void setProductId(Long productId)
    {
        this.productId = productId;
    }

    public Long getPartCount()
    {
        return partCount;
    }

    public void setPartCount(Long partCount)
    {
        this.partCount = partCount;
    }

    public Long getConsumablePartCount()
    {
        return consumablePartCount;
    }

    public void setConsumablePartCount(Long consumablePartCount)
    {
        this.consumablePartCount = consumablePartCount;
    }

    public Long getCustomPartCount()
    {
        return customPartCount;
    }

    public void setCustomPartCount(Long customPartCount)
    {
        this.customPartCount = customPartCount;
    }

    public BigDecimal getPrice()
    {
        return price;
    }

    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    @Override
    public String toString()
    {
        return "ProductVo{" +
            "productId=" + productId +
            ", partCount=" + partCount +
            ", consumablePartCount=" + consumablePartCount +
            ", customPartCount=" + customPartCount +
            ", price=" + price +
            '}';
    }
}
