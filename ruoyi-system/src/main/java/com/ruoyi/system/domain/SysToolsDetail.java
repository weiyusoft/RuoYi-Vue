package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 sys_tools_detail
 *
 * @author ruoyi
 * @date 2022-08-08
 */
public class SysToolsDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工具id */
    private Long toolId;

    /** 工具清单id */
    @Excel(name = "工具清单id")
    private Long parentId;

    /** 工具名称 */
    @Excel(name = "工具名称")
    private String toolName;

    /** 工具数量 */
    @Excel(name = "工具数量")
    private Long toolCount;

    /** 工具型号 */
    @Excel(name = "工具型号")
    private String toolNo;

    public void setToolId(Long toolId)
    {
        this.toolId = toolId;
    }

    public Long getToolId()
    {
        return toolId;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }
    public void setToolName(String toolName)
    {
        this.toolName = toolName;
    }

    public String getToolName()
    {
        return toolName;
    }
    public void setToolCount(Long toolCount)
    {
        this.toolCount = toolCount;
    }

    public Long getToolCount()
    {
        return toolCount;
    }
    public void setToolNo(String toolNo)
    {
        this.toolNo = toolNo;
    }

    public String getToolNo()
    {
        return toolNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("toolId", getToolId())
            .append("parentId", getParentId())
            .append("toolName", getToolName())
            .append("toolCount", getToolCount())
            .append("toolNo", getToolNo())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}
