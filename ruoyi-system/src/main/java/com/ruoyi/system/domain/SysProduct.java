package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品/报价单对象 sys_product
 *
 * @author zuolei
 * @date 2022-07-31
 */
public class SysProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 产品id/报价单id */
    private Long productId;

    /** 产品编号 */
    @Excel(name = "产品编号")
    private String productNo;

    /** 中文名称 */
    @Excel(name = "中文名称")
    private String name;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String englishName;

    /** 中文描述 */
    @Excel(name = "中文描述")
    private String description;

    /** 英文描述 */
    @Excel(name = "英文描述")
    private String englishDescription;

    /** 产品总价 */
    @Excel(name = "产品总价")
    private BigDecimal price;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 项目id(type=2时才有效) */
    @Excel(name = "项目id(type=2时才有效)")
    private Long projectId;

    /** 类型：1-产品 2-报价单 */
    @Excel(name = "类型：1-产品 2-报价单")
    private String type;

    /**
     * 导出标志（0可以导出 2不可以导出）
     */
    private String export;

    public void setProductId(Long productId)
    {
        this.productId = productId;
    }

    public Long getProductId()
    {
        return productId;
    }
    public void setProductNo(String productNo)
    {
        this.productNo = productNo;
    }

    public String getProductNo()
    {
        return productNo;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setEnglishName(String englishName)
    {
        this.englishName = englishName;
    }

    public String getEnglishName()
    {
        return englishName;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }
    public void setEnglishDescription(String englishDescription)
    {
        this.englishDescription = englishDescription;
    }

    public String getEnglishDescription()
    {
        return englishDescription;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    public Long getProjectId()
    {
        return projectId;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public String getExport()
    {
        return export;
    }

    public void setExport(String export)
    {
        this.export = export;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("productId", getProductId())
            .append("productNo", getProductNo())
            .append("name", getName())
            .append("englishName", getEnglishName())
            .append("description", getDescription())
            .append("englishDescription", getEnglishDescription())
            .append("price", getPrice())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("projectId", getProjectId())
            .append("type", getType())
            .toString();
    }
}
