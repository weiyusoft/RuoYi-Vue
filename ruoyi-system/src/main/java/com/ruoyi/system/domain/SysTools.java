package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工具清单管理对象 sys_tools
 * 
 * @author zuolei
 * @date 2022-08-08
 */
public class SysTools extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工具id */
    private Long toolsId;

    /** 工具清单名称 */
    @Excel(name = "工具清单名称")
    private String name;

    public void setToolsId(Long toolsId) 
    {
        this.toolsId = toolsId;
    }

    public Long getToolsId() 
    {
        return toolsId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("toolsId", getToolsId())
            .append("name", getName())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}
