package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 配件管理对象 sys_part
 *
 * @author 左磊
 * @date 2022-09-06
 */
public class SysPart extends BaseEntity implements Comparable<SysPart>
{
    private static final long serialVersionUID = 1L;

    /** 配件id */
    private Long partId;

    /** 配件编号 */
    @Excel(name = "配件编号")
    private String partNo;

    /** 分类id */
    @Excel(name = "分类id")
    private Long categoryId;

    /** 中文名称 */
    @Excel(name = "中文名称")
    private String name;

    /** 英文名称 */
    @Excel(name = "英文名称")
    private String englishName;

    /** 中文描述 */
    @Excel(name = "中文描述")
    private String description;

    /** 英文描述 */
    @Excel(name = "英文描述")
    private String englishDescription;

    /** 中文图片 */
    @Excel(name = "中文图片")
    private String picture;

    /** 英文图片 */
    @Excel(name = "英文图片")
    private String englishPicture;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 类型（1-配件，2-易损件，3-自定义配件）  */
    @Excel(name = "类型", readConverterExp = "1=-配件，2-易损件，3-自定义配件")
    private Integer type;

    /** 导出标志（0可以导出 2不可以导出） */
    @Excel(name = "导出标志", readConverterExp = "0=可以导出,2=不可以导出")
    private String export;

    /** 是否可配置易损件(0-可配置 2-不可配置) */
    @Excel(name = "是否可配置易损件(0-可配置 2-不可配置)")
    private String subPartEnable;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 简介 */
    @Excel(name = "简介")
    private String introduction;

    /** 制造商 */
    @Excel(name = "制造商")
    private String manufacturer;

    public void setPartId(Long partId)
    {
        this.partId = partId;
    }

    public Long getPartId()
    {
        return partId;
    }
    public void setPartNo(String partNo)
    {
        this.partNo = partNo;
    }

    public String getPartNo()
    {
        return partNo;
    }
    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setEnglishName(String englishName)
    {
        this.englishName = englishName;
    }

    public String getEnglishName()
    {
        return englishName;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }
    public void setEnglishDescription(String englishDescription)
    {
        this.englishDescription = englishDescription;
    }

    public String getEnglishDescription()
    {
        return englishDescription;
    }
    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getPicture()
    {
        return picture;
    }
    public void setEnglishPicture(String englishPicture)
    {
        this.englishPicture = englishPicture;
    }

    public String getEnglishPicture()
    {
        return englishPicture;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setType(Integer type)
    {
        this.type = type;
    }

    public Integer getType()
    {
        return type;
    }
    public void setExport(String export)
    {
        this.export = export;
    }

    public String getExport()
    {
        return export;
    }
    public void setSubPartEnable(String subPartEnable)
    {
        this.subPartEnable = subPartEnable;
    }

    public String getSubPartEnable()
    {
        return subPartEnable;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setIntroduction(String introduction)
    {
        this.introduction = introduction;
    }

    public String getIntroduction()
    {
        return introduction;
    }
    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("partId", getPartId())
            .append("partNo", getPartNo())
            .append("categoryId", getCategoryId())
            .append("name", getName())
            .append("englishName", getEnglishName())
            .append("description", getDescription())
            .append("englishDescription", getEnglishDescription())
            .append("picture", getPicture())
            .append("englishPicture", getEnglishPicture())
            .append("price", getPrice())
            .append("type", getType())
            .append("export", getExport())
            .append("subPartEnable", getSubPartEnable())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("createBy", getCreateBy())
            .append("introduction", getIntroduction())
            .append("manufacturer", getManufacturer())
            .toString();
    }

    @Override
    public int compareTo(SysPart part)
    {
        return (int) (this.categoryId-part.getCategoryId());
    }
}
