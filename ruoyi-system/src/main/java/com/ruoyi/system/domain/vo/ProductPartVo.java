package com.ruoyi.system.domain.vo;

import java.util.List;

public class ProductPartVo
{

    private ProductVo product;

    private List<PartVo> partList;

    public ProductVo getProduct()
    {
        return product;
    }

    public void setProduct(ProductVo product)
    {
        this.product = product;
    }

    public List<PartVo> getPartList()
    {
        return partList;
    }

    public void setPartList(List<PartVo> partList)
    {
        this.partList = partList;
    }
}
