package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.common.core.domain.entity.SysPartCategory;

/**
 * 配件分类Mapper接口
 *
 * @author zuolei
 * @date 2022-07-08
 */
public interface SysPartCategoryMapper
{

    /**
     * 查询配件分类
     *
     * @param categoryId 配件分类主键
     * @return 配件分类
     */
    public SysPartCategory selectSysPartCategoryByCategoryId(Long categoryId);

    /**
     * 查询配件分类
     *
     * @param name 配件分类中文名称或英文名称
     * @return 配件分类
     */
    public SysPartCategory selectSysPartCategoryByName(String name);

    /**
     * 查询配件分类列表
     *
     * @param sysPartCategory 配件分类
     * @return 配件分类集合
     */
    public List<SysPartCategory> selectSysPartCategoryList(SysPartCategory sysPartCategory);

    /**
     * 新增配件分类
     *
     * @param sysPartCategory 配件分类
     * @return 结果
     */
    public int insertSysPartCategory(SysPartCategory sysPartCategory);

    /**
     * 修改配件分类
     *
     * @param sysPartCategory 配件分类
     * @return 结果
     */
    public int updateSysPartCategory(SysPartCategory sysPartCategory);

    /**
     * 删除配件分类
     *
     * @param categoryId 配件分类主键
     * @return 结果
     */
    public int deleteSysPartCategoryByCategoryId(Long categoryId);

    /**
     * 批量删除配件分类
     *
     * @param categoryIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysPartCategoryByCategoryIds(Long[] categoryIds);
}
