package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysProductDetail;

/**
 * 产品/报价单关联详情Mapper接口
 *
 * @author zuolei
 * @date 2022-07-25
 */
public interface SysProductDetailMapper
{

    /**
     * 查询产品/报价单关联详情
     *
     * @param productDetailId 产品/报价单关联详情主键
     * @return 产品/报价单关联详情
     */
    public SysProductDetail selectSysProductDetailByProductDetailId(Long productDetailId);

    /**
     * 查询产品/报价单关联详情列表
     *
     * @param sysProductDetail 产品/报价单关联详情
     * @return 产品/报价单关联详情集合
     */
    public List<SysProductDetail> selectSysProductDetailList(SysProductDetail sysProductDetail);

    /**
     * 新增产品/报价单关联详情
     *
     * @param sysProductDetail 产品/报价单关联详情
     * @return 结果
     */
    public int insertSysProductDetail(SysProductDetail sysProductDetail);

    /**
     * 修改产品/报价单关联详情
     *
     * @param sysProductDetail 产品/报价单关联详情
     * @return 结果
     */
    public int updateSysProductDetail(SysProductDetail sysProductDetail);

    /**
     * 删除产品/报价单关联详情
     *
     * @param productDetailId 产品/报价单关联详情主键
     * @return 结果
     */
    public int deleteSysProductDetailByProductId(Long productDetailId);

    /**
     * 批量删除产品/报价单关联详情
     *
     * @param productDetailIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysProductDetailByProductDetailIds(Long[] productDetailIds);
}
