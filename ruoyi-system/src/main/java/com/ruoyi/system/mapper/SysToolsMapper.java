package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysTools;

/**
 * 工具清单管理Mapper接口
 * 
 * @author zuolei
 * @date 2022-08-08
 */
public interface SysToolsMapper 
{
    /**
     * 查询工具清单管理
     * 
     * @param toolsId 工具清单管理主键
     * @return 工具清单管理
     */
    public SysTools selectSysToolsByToolsId(Long toolsId);

    /**
     * 查询工具清单管理列表
     * 
     * @param sysTools 工具清单管理
     * @return 工具清单管理集合
     */
    public List<SysTools> selectSysToolsList(SysTools sysTools);

    /**
     * 新增工具清单管理
     * 
     * @param sysTools 工具清单管理
     * @return 结果
     */
    public int insertSysTools(SysTools sysTools);

    /**
     * 修改工具清单管理
     * 
     * @param sysTools 工具清单管理
     * @return 结果
     */
    public int updateSysTools(SysTools sysTools);

    /**
     * 删除工具清单管理
     * 
     * @param toolsId 工具清单管理主键
     * @return 结果
     */
    public int deleteSysToolsByToolsId(Long toolsId);

    /**
     * 批量删除工具清单管理
     * 
     * @param toolsIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysToolsByToolsIds(Long[] toolsIds);
}
