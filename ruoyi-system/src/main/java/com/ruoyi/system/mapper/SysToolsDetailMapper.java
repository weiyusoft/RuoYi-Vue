package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysToolsDetail;

/**
 * 工具管理Mapper接口
 * 
 * @author zuolei
 * @date 2022-08-08
 */
public interface SysToolsDetailMapper 
{
    /**
     * 查询工具管理
     * 
     * @param toolId 工具管理主键
     * @return 工具管理
     */
    public SysToolsDetail selectSysToolsDetailByToolId(Long toolId);

    /**
     * 查询工具管理列表
     * 
     * @param sysToolsDetail 工具管理
     * @return 工具管理集合
     */
    public List<SysToolsDetail> selectSysToolsDetailList(SysToolsDetail sysToolsDetail);

    /**
     * 新增工具管理
     * 
     * @param sysToolsDetail 工具管理
     * @return 结果
     */
    public int insertSysToolsDetail(SysToolsDetail sysToolsDetail);

    /**
     * 修改工具管理
     * 
     * @param sysToolsDetail 工具管理
     * @return 结果
     */
    public int updateSysToolsDetail(SysToolsDetail sysToolsDetail);

    /**
     * 删除工具管理
     * 
     * @param toolId 工具管理主键
     * @return 结果
     */
    public int deleteSysToolsDetailByToolId(Long toolId);

    /**
     * 批量删除工具管理
     * 
     * @param toolIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysToolsDetailByToolIds(Long[] toolIds);
}
