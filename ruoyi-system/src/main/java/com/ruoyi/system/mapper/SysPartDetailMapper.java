package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysPartDetail;

/**
 * 配件详情Mapper接口
 *
 * @author zuolei
 * @date 2022-07-09
 */
public interface SysPartDetailMapper
{

    /**
     * 查询配件详情
     *
     * @param partDetailId 配件详情主键
     * @return 配件详情
     */
    public SysPartDetail selectSysPartDetailByPartDetailId(Long partDetailId);

    /**
     * 查询配件详情列表
     *
     * @param sysPartDetail 配件详情
     * @return 配件详情集合
     */
    public List<SysPartDetail> selectSysPartDetailList(SysPartDetail sysPartDetail);

    /**
     * 新增配件详情
     *
     * @param sysPartDetail 配件详情
     * @return 结果
     */
    public int insertSysPartDetail(SysPartDetail sysPartDetail);

    /**
     * 修改配件详情
     *
     * @param sysPartDetail 配件详情
     * @return 结果
     */
    public int updateSysPartDetail(SysPartDetail sysPartDetail);

    /**
     * 删除配件详情
     *
     * @param partDetailId 配件详情主键
     * @return 结果
     */
    public int deleteSysPartDetailByPartDetailId(Long partDetailId);

    /**
     * 批量删除配件详情
     *
     * @param partDetailIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysPartDetailByPartDetailIds(Long[] partDetailIds);

    /**
     * 批量删除配件详情
     *
     * @param partIds 需要删除的partId集合
     * @return 结果
     */
    public int deleteSysPartDetailByPartIds(Long[] partIds);

    /**
     * 根据配件id查询数量
     *
     * @param partId 配件id
     * @return 数量
     */
    public int selectCountByPartId(Long partId);

    /**
     * 根据易损件id查询详情
     *
     * @param subPartId 易损件id
     * @return 配件详情
     */
    public SysPartDetail selectSysPartDetailBySubPartId(Long subPartId);
}
